import ConfigParser
import os

config = ConfigParser.RawConfigParser()
config_file = os.path.expanduser('~/.abm_barcode.ini')


def check_config_file():
    """
    Check config file and generate unless it exists.
    :return:
    """
    config.read(config_file)

    try:
        general = config.get('general', 'debug')
        is_exist = True
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
        is_exist = False

    if not is_exist:
        cfgfile = open(config_file, 'w')
        # add the settings to the structure of the file, and lets write it out...
        config.add_section('general')
        config.set('general', 'debug', 'OFF')
        config.set('general', 'debug_button', 'OFF')
        config.set('general', 'mode', 'I')
        config.set('general', 'system_id', '12345')
        config.set('general', 'name', 'Change Device Name')
        config.set('general', 'small_label_print', 'False')

        config.add_section('admin')
        config.set('admin', 'admin_password', '1357')
        config.set('admin', 'timer', '300')

        config.add_section('sql')
        config.set('sql', 'host', '192.168.18.8')
        config.set('sql', 'port', '57475')
        config.set('sql', 'username', 'raspberry')
        config.set('sql', 'password', 'raspberry')
        config.set('sql', 'database', 'ABM')
        config.set('sql', 'instance', 'DYNAMICS')

        config.add_section('user')
        config.set('user', 'activity_code', 'Cutting - Item 1')

        config.add_section('theme')
        config.set('theme', 'primary_palette', 'Blue')
        config.set('theme', 'accent_palette', 'Amber')
        config.set('theme', 'theme_style', 'Light')

        config.write(cfgfile)
        cfgfile.close()
