import os
from functools import partial
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivymd.label import MDLabel

import utils.common
import utils.db
from widgets.checkbox import LabeledCheckbox

Builder.load_file(os.path.join(os.path.dirname(__file__), 'user_settings.kv'))


class UserSettingsScreen(Screen):

    def on_enter(self, *args):
        super(UserSettingsScreen, self).on_enter(*args)
        if utils.common.get_mode() == 'P':
            Clock.schedule_once(self.update_activities)
        else:
            self.ids.container.add_widget(
                MDLabel(text='Nothing to configure for this mode',
                        font_style='Title',
                        size_hint=(None, None),
                        width=400,
                        pos_hint={'center_x': .5}
                        ))

    def update_activities(self, *args):
        """
        Retrieve all activity types and display on screen
        :param args:
        :return:
        """
        cur_code = utils.common.get_activity()
        container = self.ids.container
        container.clear_widgets()
        act_list = utils.db.get_production_activities()
        for act in act_list:
            if act != '':
                item = LabeledCheckbox(group='act', allow_no_selection=False, text=act)
                item.bind(on_checked=partial(self.select_activity, act))
                if act == cur_code:
                    item.set_value(True)
                else:
                    item.set_value(False)
                container.add_widget(item)
        container.height = LabeledCheckbox().height * (len(act_list) / 2 + 1)

    @staticmethod
    def select_activity(_name, *args):
        utils.common.set_activity(_name)
