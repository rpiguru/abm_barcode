import os
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.snackbar import Snackbar
from utils.config_util import set_config
from screens.settings.tabs.base import BaseTab
from utils import config_util
from kivy.clock import Clock

from utils.common import get_mode, set_debug_button_mode, is_debug_button, get_admin_timer, set_admin_timer, \
    get_git_commit_date
from widgets.dialog import YesNoDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'general.kv'))


class GeneralTab(BaseTab):

    dev_mode = StringProperty('I')

    def __init__(self, **kwargs):
        super(GeneralTab, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(GeneralTab, self).on_enter(*args)
        Clock.schedule_once(self.update_settings)

    def update_settings(self, *args):
        dev_name = config_util.get_config('general', 'name', 'ABM Barcode')
        self.ids.dev_name.text = dev_name
        dev_id = config_util.get_config('general', 'system_id', '12345')
        self.ids.dev_id.text = dev_id
        self.dev_mode = get_mode()
        for mode in ['I', 'P', 'T', 'R', 'A', 'E', 'F', 'In']:
            self.ids['chk_{}'.format(mode)].active = True if mode == self.dev_mode else False
        self.ids.chk_simulate_btn.active = True if is_debug_button() else False
        self.ids.admin_timeout.text = str(int(get_admin_timer()) / 60)
        self.ids.chk_small_label.active = (config_util.get_config('general', 'small_label_print', 'False').lower() == 'true')
        self.ids.updated.text = get_git_commit_date()

    def on_leave(self, *args):
        dev_name = self.ids.dev_name.text
        config_util.set_config('general', 'name', dev_name)
        dev_id = self.ids.dev_id.text
        config_util.set_config('general', 'system_id', dev_id)
        config_util.set_config('general', 'mode', self.dev_mode)
        set_admin_timer(int(self.ids.admin_timeout.text) * 60)
        Snackbar(text="Updated general settings").show()
        super(GeneralTab, self).on_leave(*args)

    def on_check(self, chk, mode):
        if chk.active:
            self.dev_mode = mode

    @staticmethod
    def on_check_simulate(chk):
        set_debug_button_mode(chk.active)
        App.get_running_app().check_debug_button()

    def on_btn_close(self):
        popup = YesNoDialog(message='Are you sure?', title='Close app')
        popup.bind(on_confirm=self.close_app)
        popup.open()

    @staticmethod
    def close_app(*args):
        App.get_running_app().stop()

    def on_btn_logout(self):
        App.get_running_app().logout_admin()

    @staticmethod
    def on_check_small_label(chk):
        val = 'True' if chk.active else 'False'
        set_config('general', 'small_label_print', val)
