# -*- coding: utf-8 -*-
import calendar
import os
import random
import threading
import time
import traceback

from kivy.app import App
from screens.error_screen.screen import ErrorScreen

import kivy_config

from kivy.properties import StringProperty, ListProperty, ObjectProperty, NumericProperty
from kivy.uix.screenmanager import NoTransition
from functools import partial
from read_barcode.read_barcode import BarcodeReader
from kivymd.snackbar import Snackbar
from kivymd.theming import ThemeManager

from config_gen import check_config_file
from screens.menu.screen import MenuScreen
from screens.settings.screen import SettingsScreen
from screens.user_settings.screen import UserSettingsScreen
import factory_reg
from utils import time_util, config_util
from utils.common import is_debug_button, get_admin_timer, set_activity, get_theme_color, get_git_commit_date
from kivy.clock import Clock, mainthread

from utils.db import get_production_activities
import utils.config_util
from widgets.dialog import PasswordDialog
from kivy.base import ExceptionHandler, ExceptionManager


class ABMExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        print ('-- Exception: {}'.format(repr(exception)))
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.go_screen('error_screen', 'left')
        return ExceptionManager.PASS


# ExceptionManager.add_handler(ABMExceptionHandler())


class MainApp(App):

    theme_cls = ThemeManager()
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    scr_menu = ObjectProperty(None)
    scr_settings = ObjectProperty(None)
    scr_user_settings = ObjectProperty(None)
    scr_err = ObjectProperty(None)

    exception = None

    mode = StringProperty('Receiving')
    cnt_admin = NumericProperty(0)

    tr_barcode = None
    tr_stop = False

    conn = None

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self._apply_theme()
        self.load_screen()
        self.go_screen('menu', 'right')
        self.check_debug_button()
        Clock.schedule_interval(self.show_time, 1)
        self.tr_barcode = threading.Thread(target=self.start_listener)
        self.tr_barcode.start()

    def _apply_theme(self):
        theme = get_theme_color()
        self.theme_cls.primary_palette = theme['primary_palette']
        self.theme_cls.accent_palette = theme['accent_palette']
        self.theme_cls.theme_style = theme['theme_style']

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm
        try:
            # sm.transition = SlideTransition()
            sm.transition = NoTransition()
            screen = self.screens[dest_screen]
            # sm.switch_to(screen, direction=direction)
            sm.switch_to(screen)
        except Exception as e:
            print 'Failed to switch screen: {}'.format(e)
        else:
            self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = ['menu', 'settings', 'user_settings', 'error_screen']
        self.screen_names = available_screens

        self.scr_menu = MenuScreen()
        self.scr_settings = SettingsScreen()
        self.scr_user_settings = UserSettingsScreen()
        self.scr_err = ErrorScreen()
        self.screens = {
            'menu': self.scr_menu,
            'settings': self.scr_settings,
            'user_settings': self.scr_user_settings,
            'error_screen': self.scr_err,
        }

    def check_debug_button(self):
        if not is_debug_button():
            for i in range(7):
                self.scr_menu.ids['btn_debug_{}'.format(i)].disabled = True

    def on_debug_btn(self, _type):
        if _type == 'activity':
            new_code = random.randint(1, 19)
            print 'New activity code from debug button: {}'.format(new_code)
            Clock.schedule_once(partial(self.set_activity, new_code))
        elif _type == 'job_number':
            self.on_barcode('job_number', 'Job 123')
        elif _type == 'product_id':
            self.on_barcode('product_number', '1234', '5678')
        elif _type == 'item_number':
            self.on_barcode('item_number', '12345')
        elif _type == 'mode':
            self.on_barcode('mode', random.choice(['A', 'I', 'P', 'R', 'T', 'E', 'F', 'In']))
        elif _type == 'eq_id':
            self.on_barcode('equipment_id', str(random.randint(1, 19)))
        else:
            ran_str = str(random.randint(1000000, 9999999))
            self.on_barcode(_type, ran_str)

    def on_barcode(self, _type, value, seq_num=None):
        """
        Call this method when barcode is scanned
        """
        if _type == 'activity':
            threading.Thread(target=self.set_activity, args=(value, )).start()
        elif _type == 'mode':
            config_util.set_config('general', 'mode', value)
            self.scr_menu.load_section()
        else:
            Clock.schedule_once(partial(self.scr_menu.on_barcode, _type, value, seq_num))

    def set_activity(self, *args):
        """
        Call this method when barcode of new activity code(`AC12`) is scanned
        """
        code = args[0]
        new_activity = None
        try:
            code = int(code)
            activities = get_production_activities()
            try:
                new_activity = activities[code]
            except IndexError:
                new_activity = activities[1]
        except Exception as e:
            print 'Failed to get activity from server: {}'.format(e)

        if new_activity is not None:
            set_activity(new_activity)
            Clock.schedule_once(partial(self.update_activity, new_activity))

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    @mainthread
    def update_activity(self, new_activity, *args):
        if new_activity is not None:
            self.scr_menu.sections['P'].update_activity()
            Snackbar(text='New activity is set: "{}"'.format(new_activity)).show()
        else:
            print 'Failed to set activity'
            Snackbar(text='Failed to set activity, please check server connection',
                     background_color=(.8, 0, .3, .5)).show()

    def start_admin_timer(self):
        threshold = get_admin_timer() / 60
        Snackbar(text='You are logged in as an administrator.\nThis will be expired after {} minutes.'.format(
            threshold), duration=3).show()
        Clock.schedule_interval(self.admin_timer, 1)

    def admin_timer(self, *args):
        threshold = get_admin_timer()
        if self.cnt_admin < threshold:
            self.cnt_admin += 1
        else:
            self.cnt_admin = 0
            Clock.unschedule(self.admin_timer)
            Snackbar(text='Administrator privilege is expired.').show()

    def show_time(self, *args):
        local_time = time_util.get_local_time()
        str_time = local_time.strftime("%B %d, %Y  %H:%M:%S")
        self.root.ids.lb_time.text = '{}, {}'.format(calendar.day_name[local_time.weekday()][:3], str_time)

    @mainthread
    def receive_barcode(self, msg, code):
        if code['type'] == 'unknown':
            Snackbar(text="Invalid barcode read: {}".format(msg), background_color=(.8, 0, .3, .5)).show()
        elif code['type'] == 'accept':      # DEBUG only
            print '`ACCEPT` button is triggered via barcode, executing...'
            self.scr_menu.sections[self.scr_menu.current_section].on_accept()
        else:
            Snackbar(text="Barcode read: {}".format(msg)).show()
            seq_num = code['sequence_number'] if 'sequence_number' in code.keys() else None
            self.on_barcode(code['type'], code['value'], seq_num)

    def start_listener(self, *args):
        while True:
            if self.tr_stop:
                break
            try:
                a = BarcodeReader()
            except IOError:
                time.sleep(1)
                continue
            else:
                code = a.read_barcode()
                if code is not None:
                    print 'Barcode read: {}'.format(code)
                    self.receive_barcode(code, decode_barcode(code))
                else:
                    pass

    def stop(self, *largs):
        super(MainApp, self).stop(*largs)
        os.kill(os.getpid(), 9)

    def on_admin(self):
        if self.cnt_admin > 0:
            self.go_screen('settings', 'left')
        else:
            pwd = utils.config_util.get_config('admin', 'admin_password', '1357')
            dlg = PasswordDialog(pwd=pwd)
            dlg.bind(on_success=self.on_success_login)
            dlg.open()

    def on_success_login(self, *args):
        self.start_admin_timer()
        self.go_screen('settings', 'left')

    def logout_admin(self):
        self.cnt_admin = 0
        Clock.unschedule(self.admin_timer)
        self.go_screen('menu', 'right')


def decode_barcode(code):
    """
    Decode received barcode
    """
    code = str(code)
    _result = {'type': 'unknown', 'value': code}
    try:
        if code.startswith('ID'):
            _result = {'type': 'user_id', 'value': code[2:]}
        elif code.startswith('JB'):
            val = code[2:]
            if val.startswith('!'):
                val = val[1:]
            _result = {'type': 'job_number', 'value': val}
        elif code.startswith('IT'):
            _result = {'type': 'item_number', 'value': code[2:]}
        elif code.startswith('PD'):
            try:
                job_number, sequence_number = code[2:].split()
            except ValueError:
                try:
                    job_number, sequence_number = code[2:].split('_')
                except ValueError:
                    job_number = code[2:]
                    sequence_number = ''
            _result = {'type': 'product_number', 'value': job_number, 'sequence_number': sequence_number}
        elif code.startswith('AC'):
            _result = {'type': 'activity', 'value': code[2:]}
        elif code.startswith('EQ'):
            _result = {'type': 'equipment_id', 'value': code[2:]}
        elif code == 'MOINVE':
            _result = {'type': 'mode', 'value': 'In'}
        elif code.startswith('MO'):
            # Available values are `MOACCCE`, `MOINAL`, `MOPROD`, `MORECE`, `MOTIME`, `MOEQUI`, `MOFINA`
            # We only need the 1st character of the mode name... lol
            _result = {'type': 'mode', 'value': code[2]}
        elif code == 'accept':      # DEBUG only
            _result = {'type': 'accept'}

    except Exception as er:
        print 'Failed to decode barcode: {}'.format(er)

    print 'Decoding barcode: {} => {}'.format(code, _result)
    return _result


if __name__ == '__main__':

    print 'PID: {}'.format(os.getpid())

    # sys.stdout = open(os.path.dirname(os.path.realpath(__file__)) + '/stdout.txt', 'w')

    check_config_file()

    print "Starting ABM Barcode GUI Application..."
    print "Last update: {}".format(get_git_commit_date())

    app = MainApp()
    app.run()
