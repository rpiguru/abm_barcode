import os
import threading
from kivy.lang import Builder
from kivy.clock import Clock, mainthread
from functools import partial
from kivy.config import _is_rpi
from kivymd.label import MDLabel
from kivymd.snackbar import Snackbar
from screens.menu.sections.base import BaseSection
from utils.db import search_item
from widgets.inventory.inventory_form import InventoryForm

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'inventory.kv'))


class InventorySection(BaseSection):

    mode = 'Inventory'
    form = None

    def on_btn_search(self):
        item_number = self.ids.item_number.text.strip()
        bin_number = self.ids.bin_number.text.strip()
        item_desc = self.ids.item_description.text.strip()
        if item_number == '' and bin_number == '' and item_desc == '':
            Snackbar(text="Please input keyword.", background_color=(.8, 0, .3, .5)).show()
            return
        self.clear_user_id()
        self.loading_dlg.open()
        threading.Thread(target=self.perform_search, args=(item_number, bin_number, item_desc, )).start()

    def on_enter(self, *args):
        super(InventorySection, self).on_enter(*args)
        # if not _is_rpi:
        #     threading.Thread(target=self.perform_search, args=('i', '', '',)).start()

    def perform_search(self, item_number, bin_number, item_desc, *args):
        items = search_item(item_number, bin_number, item_desc)
        Clock.schedule_once(partial(self.update_result, items))

    @mainthread
    def update_result(self, items, *args):
        container = self.ids.container
        container.clear_widgets()
        if items is None:
            container.add_widget(MDLabel(text='No Result', font_style='Title', halign='center'))
            container.height = container.parent.height
        else:
            self.form = InventoryForm(items=items, size=container.size, screen=self)
            container.add_widget(self.form)
        self.loading_dlg.dismiss()

    def set_item_value(self, _key, _value, ignore_error=False):
        if _key == 'user_id':
            if self.form:
                self.form.update_user_id(_value)
            self.ids.user_id.set_value(_value)
            self.ids.user_id.cursor = (0, 0)

    def clear_user_id(self):
        self.ids.user_id.text = ''

    def get_user_id(self):
        return self.ids.user_id.text
