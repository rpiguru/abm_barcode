import os
from kivy.properties import NumericProperty, StringProperty, ObjectProperty
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder

Builder.load_file(os.path.join(os.path.dirname(__file__), 'fc_item.kv'))


class FCItem(BoxLayout):

    _id = NumericProperty()
    fc_id = NumericProperty()
    desc = StringProperty()

    value = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(FCItem, self).__init__(**kwargs)
        Clock.schedule_once(self.update_value)

    def update_value(self, *args):
        if self.value is not None:
            self.ids.lb_status.text = ['NO', 'O', 'F', 'NA'][self.value]
            self.ids.lb_status.color = [0, 1, 0, 1]
            self.ids.lb_status.font_size = 15

    def get_value(self):
        return self.value


class FCCheckItem(BoxLayout):
    _id = NumericProperty()
    fc_id = NumericProperty()
    desc = StringProperty()
    value = ObjectProperty()

    def __init__(self, **kwargs):
        super(FCCheckItem, self).__init__(**kwargs)
        Clock.schedule_once(self.update_value)

    def update_value(self, *args):
        if self.value is not None:
            for i in range(4):
                self.ids['chk_{}'.format(i)].active = True if i == self.value else False

    def set_value(self, val):
        self.value = val
        self.update_value()

    def get_value(self):
        for i in range(4):
            if self.ids['chk_{}'.format(i)].active:
                return i
