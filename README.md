# ABM Barcode Scanner
===========================================================================

1. Install Kivy on RPi

    https://kivy.org/docs/installation/installation-rpi.html

2. Install dependencies

        cd ~
        git clone https://bitbucket.org/rbrodeur123/abm_barcode

        sudo apt-get install freetds-dev python-dev
        sudo apt-get install cups libcups2-dev libcupsimage2-dev g++ cups-client

        cd abm_barcode
        sudo pip install -r requirements.txt
         
        cd ~
        git clone https://bitbucket.org/rpi_guru/kivymd
        cd kivymd
        sudo python setup.py install


3. Create desktop shortcut.

        nano ~/Desktop/abm_barcode.desktop

    And add this:

        [Desktop Entry]
        Name=ABM Barcode
        Comment=ABM Barcode
        Icon=/home/pi/abm_barcode/images/logo.png
        Exec=sudo /usr/bin/python /home/pi/abm_barcode/main.py >/home/pi/abm_barcode/stdout.txt 2>&1
        Type=Application
        Encoding=UTF-8
        Terminal=true
        Categories=None;

4. Make desktop icon larger.

        nano ~/.config/libfm/libfm.conf

    If there is no such file, open another file.

        sudo nano /etc/xdg/libfm/libfm.conf

    In the `[ui]` section, change the `pane_icon_size` to `48`.

5. Enable single click.

        sudo nano .config/libfm/libfm.conf

    Change

        single_click=0
    to

        single_clsick=1

6. Increase GPU

        sudo raspi-config

   Go to `Advanced` => `Split Memory`, and set gpu size as **384**.


7. Register Label Printer
    
- Install dependencies and driver

        cd ~
        wget http://download.dymo.com/dymo/Software/Download%20Drivers/Linux/Download/dymo-cups-drivers-1.4.0.tar.gz
        tar zxvf dymo-cups-drivers-1.4.0.tar.gz
        cd dymo-cups-drivers-1.4.0
        sudo ./configure
        sudo make 
        sudo make install
        
- Configure CUPS service
    
    Follow this:
    
    https://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/

