import os
from kivy.lang import Builder
from kivy.properties import ObjectProperty

from kivymd.tabs import MDTab

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseTab(MDTab):

    screen_widget = ObjectProperty(None)

    def on_tab_press(self, *args):
        super(BaseTab, self).on_tab_press(*args)
        self.screen_widget.cur_tab = self
