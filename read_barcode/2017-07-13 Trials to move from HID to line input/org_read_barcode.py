import os
import time

hid = {4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k', 15: 'l',
       16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u', 25: 'v', 26: 'w', 27: 'x',
       28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5', 35: '6', 36: '7', 37: '8', 38: '9', 39: '0',
       # 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: '\', 51: ';', 52: "\", 53: '~', 54: ',', 55: '.', 56: '/'}
       44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: 'a', 51: ';', 52: "a", 53: '~', 54: ',', 55: '.', 56: '/'}

hid2 = {4: 'A', 5: 'B', 6: 'C', 7: 'D', 8: 'E', 9: 'F', 10: 'G', 11: 'H', 12: 'I', 13: 'J', 14: 'K', 15: 'L',
        16: 'M', 17: 'N', 18: 'O', 19: 'P', 20: 'Q', 21: 'R', 22: 'S', 23: 'T', 24: 'U', 25: 'V', 26: 'W', 27: 'X',
        28: 'Y', 29: 'Z', 30: '!', 31: '@', 32: '#', 33: '$', 34: '%', 35: '^', 36: '&', 37: '*', 38: '(', 39: ')',
        44: ' ', 45: '_', 46: '+', 47: '{', 48: '}', 49: '|', 51: ':', 52: '"', 53: '~', 54: '<', 55: '>', 56: '?'}

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

"""
Read something:  [2, 0, 12, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [2, 0, 23, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [0, 0, 30, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]
Read something:  [0, 0, 35, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]
Read something:  [0, 0, 32, 0, 0, 0, 0, 0]
Read something:  [0, 0, 33, 0, 0, 0, 0, 0]
Read something:  [0, 0, 31, 0, 0, 0, 0, 0]
Read something:  [0, 0, 40, 0, 0, 0, 0, 0]
Barcode read: IT!0600342
"""


def get_port_of_barcode_reader():
    """
    Get port name of read_barcode reader
    """
    pipe = os.popen('dmesg | grep hidraw')
    data = pipe.read().strip().splitlines()
    pipe.close()
    for line in data[::-1]:
        if 'Barcode' in line:
            num = line.split('hidraw')[1][0]
            return '/dev/hidraw{}'.format(num)
    return '/dev/hidraw0'


class BarcodeReader:

    port = '/dev/hidraw0'
    fp = None

    def __init__(self):
        self.port = get_port_of_barcode_reader()
        self.fp = open(self.port, 'rb')

    def read_barcode(self):
        ss = ""
        done = False
        buf = None

        while not done:
            try:
                # Get the character from the HID
                buf = self.fp.read(8)
                if len(buf) > 0:
                    buf = [int(ord(c)) for c in buf]
                    # print 'Read something: ', buf
                    if buf[2] == 40:
                        done = True
                        break
                    elif buf[2] > 0:
                        if buf[0] == 0:
                            ss += hid[buf[2]]
                        elif buf[0] == 2:
                            ss += hid2[buf[2]]
            except (KeyError, ValueError, IndexError):
                print 'Invalid barcode: {}'.format(buf)
                continue
        return ss


if __name__ == '__main__':

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    while True:
        try:
            a = BarcodeReader()
        except IOError:
            time.sleep(1)
            continue
        else:
            code = a.read_barcode()
            if code is not None:
                print 'Barcode read: {}'.format(code)
            else:
                pass
