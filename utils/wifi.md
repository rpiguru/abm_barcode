Here are steps I figured out to use **wpa_supplicant** directly on the terminal:

1. Add destinated **SSID** and **password** to `/etc/wpa_supplicant/wpa_supplicant.conf`:

        wpa_passphrase "TP-LINK_REAL3D1F" "1234567890" > /etc/wpa_supplicant/wpa_supplicant.conf

    After this command, we need original 2 lines(below) to ensure this working when PIXEL GUI returns:
    
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1

    So, maybe need to add these 2 lines to this file manually like this:
        
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        network={
	        ssid="TP-LINK_REAL3D1F"
         	#psk="1234567890"
         	psk=ddd6ef72cd4c2c103f8e68a98cae69d0578e6fb7dbf20f0bc894440804610d9e
        }

2. After adding this, need to clear the old lock file:
        
        rm /var/run/wpa_supplicant/wlan0
    (This file prevents to initialize **wpa_supplicant** if we had failed to connect to an AP before)

    And, now, initialize **wpa_supplicant** with this:
 
        wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf

3. Start connecting to the AP

        dhclient wlan0

    If SSID and password are correct, this will be finished in 1~2 seconds(really cool!), otherwise, this will not be finished for over 10 sec. (I tested for several times by giving incorrect passwords manually)
    
    So, the python script would monitor this process and raise timeout error thinking that the password is incorrect.

**TODO**: Need to test when the password is not needed.
