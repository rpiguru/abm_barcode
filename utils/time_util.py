"""
    Utility for date & time operations
"""
import glob
import config_util
import datetime
from kivy.config import _is_rpi
import pytz


def get_current_timezone():
    """
    Get Olson timezone name from ini file
    :return:
    """
    return config_util.get_config('general', 'timezone')


def set_current_timezone(tz):
    """
    Save new timezone value to ini file
    :param tz:
    :return:
    """
    return config_util.set_config('general', 'timezone', tz)


def get_timezone():
    str_tz = config_util.get_config('general', 'timezone')
    if str_tz is None:
        return pytz.UTC
    else:
        return pytz.timezone(str_tz)


def get_local_time():
    """
    Get local time based on the timezone setting in `aware.ini`
    :return:
    """
    local_time = datetime.datetime.now()
    return local_time


def to_utc_datetime(local_dt):
    """
    Convert local datetime to utc datetime
    """
    tz = pytz.timezone(config_util.get_config('general', 'timezone'))
    return tz.normalize(tz.localize(local_dt)).astimezone(pytz.utc)


def get_all_timezone_names():
    """
    Get Olson timezone names
    :return: List of Olson timezone names
    """
    continental_list = ['US', 'Canada']

    zone_list = []
    if _is_rpi:
        for cont in continental_list:
            for zone in glob.glob('/usr/share/zoneinfo/{}/*'.format(cont)):
                zone_list.append('{}/{}'.format(*(zone.split('/')[-2:])))
    else:
        zone_list = ['US/Aleutian', 'US/Arizona', 'US/Indiana-Starke', 'US/Mountain', 'US/Pacific', 'US/Samoa',
                     'US/Hawaii', 'US/Central', 'US/Eastern', 'US/Alaska', 'US/East-Indiana', 'US/Michigan',
                     'US/Pacific-New', 'Canada/Atlantic', 'Canada/Eastern', 'Canada/Mountain', 'Canada/Pacific',
                     'Canada/Yukon', 'Canada/Central', 'Canada/East-Saskatchewan', 'Canada/Newfoundland',
                     'Canada/Saskatchewan']

    # Ignore timezone that is not supported by `pytz` package.
    tz_list = []
    for tz in zone_list:
        try:
            pytz.timezone(tz)
            tz_list.append(tz)
        except pytz.UnknownTimeZoneError:
            pass
    return tz_list


timezone_list = get_all_timezone_names()


if __name__ == '__main__':

    print get_all_timezone_names()
