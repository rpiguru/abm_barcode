import os
import threading
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, partial
from kivy.uix.screenmanager import Screen, NoTransition
from kivymd.card import MDCard
from kivymd.snackbar import Snackbar
from screens.menu.sections.access import AccessSection
from screens.menu.sections.equipment import EquipmentSection
from screens.menu.sections.final_check import FinalCheckSection
from screens.menu.sections.inventory import InventorySection
from screens.menu.sections.inventory_allocation import IASection
from screens.menu.sections.production import ProductionSection
from screens.menu.sections.receive import ReceiveSection
from screens.menu.sections.time_clock import TimeClockSection
from utils.common import get_mode, get_historic_data, append_historic_data, alias_map, is_debug_button
from utils.db import get_job_description, get_item_description, get_equipment_desc

Builder.load_file(os.path.join(os.path.dirname(__file__), 'menu.kv'))


class MenuScreen(Screen):

    current_section = StringProperty('')  # Store title of current screen
    sections = {}  # Dict of all screens

    history_data = ListProperty()
    history_card = None

    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)
        self.sections = {
            'I': IASection(root_widget=self),
            'R': ReceiveSection(root_widget=self),
            'T': TimeClockSection(root_widget=self),
            'A': AccessSection(root_widget=self),
            'P': ProductionSection(root_widget=self),
            'E': EquipmentSection(root_widget=self),
            'F': FinalCheckSection(root_widget=self),
            'In': InventorySection(root_widget=self)
        }
        self.load_section()

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)
        if not is_debug_button():
            try:
                self.ids.container.remove_widget(self.ids.debug_btn_container)
            except ReferenceError:
                pass
        Clock.schedule_once(self.sections[self.current_section].on_enter)
        Clock.schedule_once(self.load_section)
        threading.Thread(target=self.load_historic_data).start()

    def load_section(self, *args):
        sm = self.ids.sm
        sm.transition = NoTransition()
        mode = get_mode()
        if mode != self.current_section:
            sm.switch_to(self.sections[mode])
            self.ids.lb_entry.text = self.sections[mode].mode.upper()
            self.current_section = mode
            if mode == 'In':
                self.history_card = None
                if len(self.ids.box.children) > 1:
                    self.ids.box.remove_widget(self.ids.box.children[0])
            elif self.history_card is None:
                self.history_card = HistoryCard()
                self.ids.box.add_widget(self.history_card)
        # mode_names = {
        #     'I': 'INVENTORY ALLOCATION',
        #     'R': 'RECEIVING',
        #     'T': 'TIME CLOCK',
        #     'A': 'ACCESS',
        #     'P': 'PRODUCTION ACTIVITY'
        # }

    @mainthread
    def on_barcode(self, _key, val, seq_num, *args):
        print 'Barcode is scanned, key: {}, val: {}, seq: {}'.format(_key, val, seq_num)
        cur_section = self.sections[self.current_section]
        cur_section.set_item_value(_key, val, ignore_error=True)
        if _key == 'user_id':
            cur_section.set_timestamp()
        elif _key == 'product_number':
            if self.current_section in ['P', 'F']:
                threading.Thread(target=self.pull_job_desc, args=(val,)).start()
        elif _key == 'job_number':
            if self.current_section in ['P', 'F']:
                cur_section.set_item_value('product_number', val, ignore_error=True)
            threading.Thread(target=self.pull_job_desc, args=(val,)).start()
        elif _key == 'equipment_id':
            if self.current_section == 'E':
                threading.Thread(target=self.pull_equipment_info, args=(val,)).start()
        # elif _key == 'item_number':
        #     threading.Thread(target=self.pull_item_desc, args=(val,)).start()
        if seq_num is not None:
            cur_section.set_item_value('sequence_number', seq_num, ignore_error=True)

    def load_historic_data(self, *args):
        self.history_data = get_historic_data()
        Clock.schedule_once(self.display_historic_data)

    def add_historic_data(self, *args):
        data = args[0]
        msg = "{},User ID: {},Mode: {}".format(data.pop('timestamp'), data.pop('user_id'), data.pop('mode'))
        for _key in data.keys():
            if type(data[_key]) != list:
                try:
                    msg += ',{}: {}'.format(alias_map[_key], data[_key])
                except KeyError:
                    pass
            else:
                if _key == 'item':
                    for item in data[_key]:
                        if self.current_section == 'F':
                            msg += ',FCID: {}   Status: {}'.format(item['fc_id'], item['status'])
                        else:
                            msg += ',Item #: {}   Qty: {}'.format(item['item_number'], item['quantity'])
        append_historic_data(msg)
        self.history_data = [msg, ] + self.history_data[:-1]
        self.display_historic_data()

    @mainthread
    def display_historic_data(self, *args):
        _txt = ''
        for line in self.history_data:
            split = line.split(',')
            _txt = _txt + '- ' + split[0] + '\n'
            for i in range(1, len(split)):
                _txt = _txt + '  ' + split[i] + '\n'
            _txt += '\n'
        if self.history_card:
            self.history_card.show_history(_txt)

    def pull_job_desc(self, *args):
        val = args[0]
        job_desc = get_job_description(val)
        Clock.schedule_once(partial(self.update_job_desc, job_desc))

    @mainthread
    def update_job_desc(self, job_desc, *args):
        print 'Got job description from DB: {}'.format(job_desc)
        cur_section = self.sections[self.current_section]
        for _key, _val in job_desc.items():
            cur_section.set_item_value(_key=_key, value=_val, ignore_error=True)

    def pull_item_desc(self, *args):
        val = args[0]
        item_desc = get_item_description(val)
        Clock.schedule_once(partial(self.update_item_desc, val, item_desc))

    @mainthread
    def update_item_desc(self, item_number, item_desc, *args):
        msg = 'An item ({}) is scanned: "{}"'.format(item_number, item_desc)
        print msg
        Snackbar(text=msg).show()

    def pull_equipment_info(self, *args):
        val = int(args[0])
        desc, status = get_equipment_desc(val)
        cur_section = self.sections[self.current_section]
        cur_section.ids.description.text = desc
        cur_section.update_status_label(status)


class HistoryCard(MDCard):

    def show_history(self, val):
        self.ids.history.text = val
