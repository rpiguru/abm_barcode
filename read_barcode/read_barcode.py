import sys


class BarcodeReader:
    def read_barcode(self):
        ss = ""
        done = False
        ch = ""
        while not done:
            # ch = input()
            ch = sys.stdin.readline()
            ch = ch.strip("\n")
            ss = ch
            if len(ss) >= 3:
                done = 1
        return ss


if __name__ == '__main__':

    # if os.geteuid() != 0:
    #    exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    while True:
        a = BarcodeReader()
        code = a.read_barcode()
        if code is not None:
            print ('Barcode read: ' + code)
        else:
            pass
