import os
import threading

from kivy.lang import Builder
from screens.menu.sections.base import BaseSection
from kivy.clock import Clock
import utils.common

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'production.kv'))


class ProductionSection(BaseSection):
    item_list = ['user_id', 'product_number', 'activity', 'timestamp', 'job_name', 'customer_name', 'division',
                 'sequence_number', 'quantity']
    mode = 'Production'

    def __init__(self, **kwargs):
        super(ProductionSection, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(ProductionSection, self).on_enter(*args)
        Clock.schedule_once(self.update_activity)

    def update_activity(self, *args):
        self.ids['activity'].set_value(utils.common.get_activity())

    def on_focus_job_number(self, *args):
        if not self.ids.product_number.focus:
            self.btn_job_number()

    def btn_job_number(self, check_blank=True):
        job_num = self.ids.product_number.get_value()
        if len(job_num.strip()) == 0 and check_blank:
            # Snackbar(text='Please input job number', background_color=(.8, 0, .3, .5)).show()
            return
        threading.Thread(target=self.pull_job_desc, args=(job_num,)).start()
