import os
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ListProperty, StringProperty, ObjectProperty, NumericProperty
from kivy.lang import Builder
from kivy.clock import Clock
from kivymd.button import MDIconButton
from functools import partial
from kivymd.card import MDSeparator
from kivymd.label import MDLabel
from kivymd.snackbar import Snackbar

from utils.db import get_detail_of_item, add_print_track
from utils.label_printer.printer import create_inventory_pdf, print_file
from widgets.inventory.inventory_item import InventoryItemPopup
from kivy.config import _is_rpi

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'inventory_form.kv'))


class InventoryForm(BoxLayout):

    items = ListProperty()
    screen = ObjectProperty()
    cur_item = None
    user_id = ''
    page_index = 0

    def __init__(self, **kwargs):
        self.page_index = 0
        self.cur_item = None
        self.user_id = ''
        super(InventoryForm, self).__init__(**kwargs)
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        if len(self.items) > 5:
            pages = PaginationBox(total_page=len(self.items) // 5 + 1)
            pages.bind(on_page=self.on_change_page)
            self.ids.bottom_box.add_widget(pages)
        self._add_items(self.items[:5])

    def _add_items(self, items):

        rows = self.ids.ly_rows
        rows.clear_widgets()
        detail = self.ids.ly_detail
        detail.clear_widgets()

        for i, item in enumerate(items):
            box = BoxLayout()
            btn_print = MDIconButton(icon='printer', pos_hint={'center_y': .5}, color=(.6, .6, .6, .6))
            btn_print.bind(on_release=partial(self.on_print_item, i))
            btn_detail = MDIconButton(icon='lead-pencil', pos_hint={'center_y': .5}, color=(.6, .6, .6, .6))
            btn_detail.bind(on_release=partial(self.on_detail_item, i))
            box.add_widget(btn_detail)
            box.add_widget(MDSeparator(width=1, orientation='vertical'))
            box.add_widget(btn_print)
            rows.add_widget(box)
            rows.add_widget(MDSeparator(height=1))

            box = BoxLayout(spacing=3)
            box.add_widget(MDLabel(font_style='Body1', text=str(item['ITEMNMBR']), halign='center', size_hint_x=.5))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Caption', text=str(item['ITEMDESC'])))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Body1',
                                   text=', '.join(
                                       ['{}/{}'.format(b['LOCNCODE'], b['BINNMBR']) for b in item['bin_loc']])))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Body1', text=str(item['BaseUofM']), halign='center', size_hint_x=.3))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Body1', text=str(item['QTYONHND00']), halign='center', size_hint_x=.3))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Body1', text=str(item['QTYONORD00']), halign='center', size_hint_x=.3))
            box.add_widget(MDSeparator(orientation='vertical'))
            box.add_widget(MDLabel(font_style='Body1', text=str(item['QTYONALL00']), halign='center', size_hint_x=.3))
            detail.add_widget(box)
            detail.add_widget(MDSeparator(height=1))

        for i in range(5 - len(items)):
            rows.add_widget(BoxLayout())
            detail.add_widget(BoxLayout())

    def on_print_item(self, index, *args):
        user_id = self.screen.get_user_id()
        if not _is_rpi:
            user_id = 'USER_ID'

        if user_id == '':
            Snackbar(text='Please scan User ID', background_color=(.8, 0, .3, .5)).show()
            return
        index = 5 * self.page_index + index
        part_num = self.items[index]['ITEMNMBR']
        part_desc = self.items[index]['ITEMDESC']
        bin_loc = ' '.join(['{}/{}'.format(b['LOCNCODE'], b['BINNMBR']) for b in self.items[index]['bin_loc']])
        print 'Printing label: Number - {}, Desc - {}, Bin/Loc - {}'.format(part_num, part_desc, bin_loc)
        pdf_file = create_inventory_pdf(part_num=part_num, part_desc=part_desc, bin_loc=bin_loc)
        print_file(pdf_file)
        # self.screen.clear_user_id()
        add_print_track(part_num, user_id)

    def on_detail_item(self, index, *args):
        index = 5 * self.page_index + index
        self.on_item_updated(index)
        self.cur_item = InventoryItemPopup(detail=self.items[index], screen=self.screen)
        self.cur_item.bind(on_item_updated=partial(self.on_item_updated, index))
        self.cur_item.open()

    def update_user_id(self, val):
        if self.cur_item:
            self.user_id = val
            self.cur_item.update_user_id(val)

    def on_change_page(self, *args):
        self.page_index = args[1]
        self._add_items(self.items[5 * self.page_index: 5 * self.page_index + 5])

    def on_item_updated(self, index, *args):
        self.items[index].update(get_detail_of_item(self.items[index]['ITEMNMBR']))


class PaginationBox(BoxLayout):

    cur_page = NumericProperty()
    total_page = NumericProperty()

    def __init__(self, **kwargs):
        super(PaginationBox, self).__init__(**kwargs)
        self.register_event_type('on_page')
        if self.total_page > 0:
            self.cur_page = 1

    def on_page(self, *args):
        pass

    def on_btn_prev(self):
        if self.cur_page > 1:
            self.cur_page -= 1
            self.dispatch('on_page', self.cur_page - 1)

    def on_btn_next(self):
        if self.cur_page < self.total_page:
            self.cur_page += 1
            self.dispatch('on_page', self.cur_page - 1)
