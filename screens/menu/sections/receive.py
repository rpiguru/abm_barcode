import os
from kivy.clock import Clock
from kivy.config import _is_rpi
from kivy.lang import Builder
from screens.menu.sections.base import BaseSection
from utils.label_printer.printer import create_pdf, print_file

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'receive.kv'))


class ReceiveSection(BaseSection):

    item_list = ['user_id', 'job_number', 'job_name',
                 'customer_name', 'division', 'quantity', 'timestamp'
                 ]
    mode = 'Receiving'

    def __init__(self, **kwargs):
        super(ReceiveSection, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(ReceiveSection, self).on_enter(*args)
        Clock.schedule_once(self.update_constants)

    def update_constants(self, *args):
        pass

    def on_accept(self):
        if super(ReceiveSection, self).on_accept():
            d = self._collect_data()
            pdf_file = create_pdf(customer_name=d['customer_name'],
                                  job_name=d['job_name'], _barcode=d['job_number'], div_num=d['division'])
            print_file(pdf_file, copies=d['quantity'])
