"""
    Utility for the remote MS SQL server
"""
import pymssql
import datetime
import random
from utils import config_util
from kivy.config import _is_rpi
import time

action_map = {'Enter': 'A', 'Exit': 'L', 'Unknown': 'U'}


def get_db_credentials():
    """
    Get db credentials from `config.ini`
    """
    credentials = {}
    for _key in ['host', 'port', 'username', 'password', 'database', 'instance']:
        credentials[_key] = config_util.get_config('sql', _key)
    return credentials


def set_db_credentials(credential):
    """
    Save new credential information to `config.ini`
    """
    for _key in credential.keys():
        config_util.set_config('sql', _key, credential[_key])


def connect():
    """
    :rtype: connection instance if successful connection, otherwise `None`
    """
    try:
        if _is_rpi:
            credential = get_db_credentials()
            host = credential['host']
            user = credential['username']
            password = credential['password']
            db = credential['database']
            port = int(credential['port'])
            # instance = credential['instance']
            conn = pymssql.connect(server='{}'.format(host), user=user, password=password, database=db, port=port,
                                   as_dict=True)
            return conn
        else:
            import pymysql
            conn = pymysql.connect(host='localhost', user='root', password='root', db='ABM',
                                   charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
            return conn
    except Exception as e:
        print 'Failed to connect to remote MS SQL: {}'.format(e)


def get_job_description(job_number):
    result = {'job_name': 'Unknown', 'customer_name': 'Unknown', 'division': 'Unknown'}
    conn = connect()
    if conn is not None:
        try:
            cursor = conn.cursor()
            sql = "SELECT * FROM V_Bar_Jobs WHERE JobNumber='{}';".format(job_number)
            print 'Querying job number: {}'.format(sql)
            cursor.execute(sql)
            row = cursor.fetchone()
            conn.close()
            if row is not None:
                result = {
                            'job_name': row['JobName'].strip(),
                            'customer_name': row['CustomerName'].strip(),
                            'division': row['Division'].strip()
                            }
        except Exception as e:
            print 'Failed to get job description: {}'.format(e)
        finally:
            conn.close()

    return result


def get_item_description(item_number):
    result = 'Unknown'
    conn = connect()
    if conn is not None:
        try:
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM V_Bar_Inventory WHERE ITEMNMBR={}".format(item_number))
            row = cursor.fetchone()
            conn.close()
            if row is not None:
                result = row['ITEMDESC'].strip()
        except Exception as e:
            print 'Failed to get item description: {}'.format(e)
        finally:
            conn.close()
    return result


def upload_data(d):
    conn = connect()
    if conn is None:
        return False, 'Failed to connect to DB'
    system_id = config_util.get_config('general', 'system_id', '12345')
    system_name = config_util.get_config('general', 'name', 'ABM Barcode')
    sample_data = d[0] if type(d) == list else d
    dt = datetime.datetime.strptime(sample_data['timestamp'], "%Y-%m-%d %H:%M:%S")
    mode = sample_data['mode']
    sql = ''
    if mode == 'Inventory Allocation':
        cursor = conn.cursor()
        result = True
        for item in d['item']:
            try:
                sql = "INSERT INTO T_BAR_InventoryAllocation (SystemID, SystemName, DateTime, UserID, ItemNumber, " \
                      "JobNumber, Qty) VALUES (N'{}', N'{}', CAST(N'{}' AS DateTime), N'{}', N'{}', N'{}', {});".format(
                       system_id, system_name, dt, d['user_id'], item['item_number'], d['job_number'], item['quantity'])
                print 'Uploading to db: \n   {}'.format(sql)
                cursor.execute(sql)
                conn.commit()
            except Exception as e:
                print 'Failed to execute sql query: {}'.format(e)
                result = False
        conn.close()
        return result, result
    elif mode == 'Receiving':
        sql = "INSERT INTO T_BAR_Receiving (SystemID, SystemName, DateTime, UserID, JobNumber, Qty) " \
              "VALUES (N'{}', N'{}', CAST(N'{}' AS DateTime), N'{}', '{}', {});".format(
                system_id, system_name, dt, d['user_id'], d['job_number'], d['quantity'])
    elif mode == 'Time Clock':
        sql = "INSERT INTO T_BAR_TimeClock (SystemID, SystemName, DateTime, UserID, InOrOut) " \
              "VALUES (N'{}', N'{}', CAST(N'{}' AS DateTime), N'{}', N'{}');".format(
                system_id, system_name, d['timestamp'], d['user_id'], d['user_action'][0])
    elif mode == 'Access':
        sql = "INSERT INTO T_BAR_Access (SystemID, SystemName, DateTime, UserID, ArriveOrLeavingOrUnknown) " \
              "VALUES ('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}');".format(
                system_id, system_name, dt, d['user_id'], action_map[d['user_action']])
    elif mode == 'Production':
        try:
            code = get_production_activities().index(d['activity'])
            activity_id = code
        except ValueError:
            activity_id = 1
        if 'sequence_number' not in d.keys():
            try:
                seq = d['product_number'].split()[1]
                seq = int(seq)
            except (IndexError, ValueError):
                seq = -1
        else:
            try:
                seq = int(d['sequence_number'])
            except ValueError:
                seq = -1
        sql = "INSERT INTO T_Bar_Activity (SystemID, SystemName, DateTime, UserID, JobNumber, ActivityID, " \
              "Sequence, Qty) VALUES ('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', {}, {}, {});".format(
                system_id, system_name, dt, d['user_id'], d['product_number'], activity_id, seq, d['quantity'])

    elif mode == 'Equipment':
        sql = "INSERT INTO T_BAR_EQ_Activity (EQID, SystemID, SystemName, DateTime, UserID, ReturnTakeBroken, " \
              "Notes, JobNumber) " \
              "VALUES ({}, '{}', '{}',CAST(N'{}' AS DateTime), '{}', '{}', '{}', N'{}');".format(
                d['equipment_id'], system_id, system_name, dt, d['user_id'], d['status'], d['note'], d['job_number'])

    elif mode == 'Final Check':
        cursor = conn.cursor()
        if 'sequence_number' not in d.keys():
            try:
                seq = d['product_number'].split()[1]
                seq = int(seq)
            except (IndexError, ValueError):
                seq = -1
        else:
            try:
                seq = int(d['sequence_number'])
            except ValueError:
                seq = -1

        result = True
        for item in d['item']:
            try:
                sql = "INSERT INTO T_BAR_FC_Activity (SystemID, SystemName, DateTime, UserID, FCID, " \
                      "FC0notok1ok2fixed3na, JobNumber, Sequence) VALUES " \
                      "('{}', '{}', CAST(N'{}' AS DateTime), '{}', {}, {}, '{}', {});".format(
                       system_id, system_name, dt, d['user_id'], item['fc_id'], item['status'],
                       d['product_number'], seq)
                print 'Uploading FC Item to db: \n   {}'.format(sql)
                cursor.execute(sql)
                conn.commit()
            except Exception as e:
                print 'Failed to execute sql query: {}'.format(e)
                result = False
        try:
            item = d['item'][0]
            sql = "INSERT INTO T_BAR_FC_Activity (SystemID, SystemName, DateTime, UserID, FCID, " \
                  "FC0notok1ok2fixed3na, Notes, JobNumber, Sequence) VALUES " \
                  "('{}', '{}', CAST(N'{}' AS DateTime), '{}', 0, {}, '{}', '{}', {});".format(
                    system_id, system_name, dt, d['user_id'], item['status'], d['note'], d['product_number'], seq)
            print 'Uploading Note of FC Item to db: \n   {}'.format(sql)
            cursor.execute(sql)
            conn.commit()
        except Exception as e:
            print 'Failed to execute sql query: {}'.format(e)
            result = False
        conn.close()
        return result, result

    cursor = conn.cursor()
    print 'Uploading to db: \n   {}'.format(sql)
    try:
        cursor.execute(sql)
        conn.commit()
    except Exception as e:
        print 'Failed to execute sql query: {}'.format(e)
        return False, e.args[0]
    conn.close()
    return True, 'OK'


def get_production_activities():
    """
    Download list of production activities from the server
    """
    # return ['', 'Cutting - Item 1', 'Cutting - Item 2', 'Cutting - Item 3', 'CNC', 'Prep', 'Assembly - Frame',
    #         'Assembly - Hardware', 'Assembly - Slider', 'Bank - Box', 'Bank - Bow', 'Bank - Bay',
    #         'Bank Patio Door (2 Panel)', 'Bank Patio Door (3 Panel)', 'Special', 'Glaze', 'Inspect',
    #         'Wrapped and moved to storage area', 'Moved to storage area', 'Loaded on trailer'
    #         ]
    try:
        conn = connect()
        if conn is None:
            return []
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM T_Bar_Activities;")
        rows = cursor.fetchall()
        conn.close()
        rows = sorted(rows, key=lambda k: k['ActivityID'])
        acts = [row['ActivityDescription'].strip() for row in rows]
        return ['', ] + acts        # Activity numbers starts from 1
    except Exception:
        return ['', ]


def get_equipment_desc(eq_id):
    """
    Get the detailed description & equipment status from the db
    :param eq_id:
    :return:
    """
    if _is_rpi:
        try:
            conn = connect()
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute("SELECT EQDesc FROM T_Bar_EQ_Inventory where EQID={};".format(eq_id))
                rows = cursor.fetchall()
                desc = rows[0]['EQDesc'] if len(rows) > 0 else 'N/A'
                cursor.execute("SELECT * FROM T_Bar_EQ_Activity where EQID={} ORDER BY 'DateTime' DESC;".format(eq_id))
                rows = cursor.fetchall()
                status = rows[0]['ReturnTakeBroken'] if len(rows) > 0 else 'R'
                conn.close()
                print 'Got EQ desc of {}: {}, status: {}'.format(eq_id, desc, status)
                return desc, status
        except Exception as e:
            print 'Failed to get EQ desc: {}'.format(e)
        return 'N/A', 'R'
    else:
        return 'This is the long description' * 10, 'R'


def get_fc_verification_list():
    if _is_rpi:
        try:
            conn = connect()
            if conn is None:
                return []
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM T_Bar_FC_Items WHERE FCUse=1 ORDER BY 'FCOrder';")
            rows = cursor.fetchall()
            rows = sorted(rows, key=lambda k: k['FCOrder'])
            conn.close()
            # print 'Getting FC Verification list: {}'.format(rows)
            return rows
        except Exception as e:
            print 'Failed to get fc verification list: {}'.format(e)
            return []
    else:
        return [
            {'ID': 0, 'FCID': 0, 'FCOrder': 2134, 'FCDesc': 'Description #1' * 8, 'FCUse': True},
            {'ID': 1, 'FCID': 323, 'FCOrder': 4134, 'FCDesc': 'Description #2' * 5, 'FCUse': True},
            {'ID': 2, 'FCID': 325, 'FCOrder': 2534, 'FCDesc': 'Description #3' * 6, 'FCUse': True},
            {'ID': 3, 'FCID': 327, 'FCOrder': 2234, 'FCDesc': 'Description #4' * 4, 'FCUse': True},
            {'ID': 4, 'FCID': 334, 'FCOrder': 2164, 'FCDesc': 'Description #5' * 2, 'FCUse': True},
            {'ID': 5, 'FCID': 329, 'FCOrder': 2137, 'FCDesc': 'Description #6' * 7, 'FCUse': True},
            {'ID': 6, 'FCID': 320, 'FCOrder': 2138, 'FCDesc': 'Description #7' * 4, 'FCUse': True},
        ]


def search_item(item_number, bin_number, item_desc):
    try:
        s_time = time.time()
        conn = connect()
        if conn is None:
            return []
        cursor = conn.cursor()

        items_with_bin = []
        if bin_number != '':
            query = "SELECT * FROM V_Bar_InventoryBin WHERE BINNMBR like '%{}%'".format(bin_number)
            print 'Searching by bin numbers: `{}`'.format(query)
            cursor.execute(query)
            bins = cursor.fetchall()
            items_with_bin = [b['ITEMNMBR'] for b in bins]
            print 'Items with given bin numbers: {}'.format(items_with_bin)

        query = "SELECT * FROM V_Bar_InventoryFull"
        like_list = []
        if item_number != '':
            like_list.append("ITEMNMBR like '%{}%'".format(item_number))
        if item_desc != '':
            like_list.append("ITEMDESC like '%{}%'".format(item_desc))
        if len(items_with_bin) > 0:
            like_list.append("ITEMNMBR in ({})".format(','.join(["'{}'".format(b) for b in items_with_bin])))

        if len(like_list) > 0:
            query += " WHERE {}".format(' or '.join(like_list))
        else:
            conn.close()
            return []
        print 'Searching items: `{}`'.format(query)
        cursor.execute(query)
        items = cursor.fetchall()
        for item in items:
            for _key in ['QTYONHND00', 'QTYONORD00', 'QTYONALL00']:
                item[_key] = round(float(item[_key]), 1)
            item.update(get_detail_of_item(item['ITEMNMBR'], cursor))

        items.sort(key=lambda m: m['ITEMNMBR'])

        print '======= Search result ({} items, elapsed {} sec) ======='.format(len(items), time.time() - s_time)
        print 'Query: Item Number: {}, Bin Number: {}, Item Desc: {}'.format(item_number, bin_number, item_desc)
        print 'Result: {}'.format(items)
        conn.close()
        if not _is_rpi:
            items += get_constant_search_result()
        return items
    except Exception as e:
        print 'Failed to search item: {}'.format(e)


def get_detail_of_item(item_number, cur_cursor=None):
    if cur_cursor is None:
        conn = connect()
        if conn is None:
            return {}
        cursor = conn.cursor()
    else:
        cursor = cur_cursor
        conn = None

    # Get LOC/BIN for each item
    query = "SELECT * FROM V_Bar_InventoryBin WHERE ITEMNMBR='{}'".format(item_number)
    print 'Getting LOC/BIN for each item: `{}`'.format(query)
    cursor.execute(query)
    bins = cursor.fetchall()
    bin_loc = [{
        'LOCNCODE': b['LOCNCODE'].strip(),
        'BINNMBR': b['BINNMBR'].strip(),
    } for b in bins if b['LOCNCODE'].strip() != '' and b['BINNMBR'].strip() != '']
    print 'Found LOC/BIN: {}'.format(bin_loc)

    if _is_rpi:
        query = "SELECT TOP 1 * from T_Bar_InventoryUpdateQty WHERE " \
                "ItemNumber='{}' ORDER BY DateTime DESC ".format(item_number)
    else:
        query = "SELECT * from T_Bar_InventoryUpdateQty WHERE " \
                "ItemNumber='{}' ORDER BY DateTime DESC ".format(item_number)
    print 'Getting QtyOHPending: `{}`'.format(query)
    cursor.execute(query)
    rows = cursor.fetchall()
    if rows and not rows[0].get('Pushed'):
        try:
            QtyOHPending = round(rows[0].get('QtyNew'), 1)
        except (ValueError, TypeError):
            QtyOHPending = ''
    else:
        QtyOHPending = ''

    for b in bin_loc:
        if _is_rpi:
            query = "SELECT TOP 1 * from T_Bar_InventoryUpdateBin WHERE ItemNumber='{}' and LOCNCODE='{}' " \
                    "ORDER BY DateTime DESC ".format(item_number, b['LOCNCODE'])
        else:
            query = "SELECT * from T_Bar_InventoryUpdateBin WHERE ItemNumber='{}' and LOCNCODE='{}' " \
                    "ORDER BY DateTime DESC ".format(item_number, b['LOCNCODE'])
        cursor.execute(query)
        rows = cursor.fetchall()
        if rows and not rows[0].get('Pushed'):
            b['BinLocPending'] = rows[0]['BINNMBRNew']
        else:
            b['BinLocPending'] = ''

    if conn:
        conn.close()

    updated = {'QtyOHPending': QtyOHPending, 'bin_loc': bin_loc}

    print 'Got detail of {}: {}'.format(item_number, updated)
    return updated


def get_constant_search_result():
    result = []
    for i in range(20):
        bin_len = random.randint(1, 5)
        result.append(
            {
                'ITEMNMBR': 'Item # {}'.format(i),
                'ITEMDESC': 'Item Description {}'.format(i),
                'BaseUofM': 'Base Unit of {}'.format(i),
                'QTYONHND00': random.randint(1, 100000) / 100.0,
                'QTYONORD00': random.randint(1, 100000) / 100.0,
                'QTYONALL00': random.randint(1, 10000) / 100.0,
                'VENDNAME': 'Vendor Name {}'.format(i),
                'VENDITNM': 'Vendor # {}'.format(i),
                'QtyOHPending': random.choice([0, random.randint(0, 1000) / 10]),
                'bin_loc': [
                    {
                        'LOCNCODE': 'LOC{}'.format(k),
                        'BINNMBR': 'BIN#{}'.format(k),
                        'BinLocPending': random.choice(['', str(random.randint(0, 100))])
                    } for k in range(bin_len)],
            }
        )
    return result


def add_inventory_update_qty(user_id, item_number, qty_current, qty_new):
    system_id = config_util.get_config('general', 'system_id', '12345')
    system_name = config_util.get_config('general', 'name', 'ABM Barcode')
    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if True:
    # try:
        conn = connect()
        if conn is None:
            return 'Failed to connect'
        cursor = conn.cursor()
        if qty_current == '':
            sql = "INSERT INTO T_Bar_InventoryUpdateQty (SystemID, SystemName, DateTime, UserID, ItemNumber, " \
                  "QtyNew) VALUES " \
                  "('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', {});".format(
                    system_id, system_name, dt, user_id, item_number, qty_new)
        else:
            sql = "INSERT INTO T_Bar_InventoryUpdateQty (SystemID, SystemName, DateTime, UserID, ItemNumber, " \
                  "QtyCurrent, QtyNew) VALUES " \
                  "('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', {}, {});".format(
                    system_id, system_name, dt, user_id, item_number, qty_current, qty_new)

        print 'Uploading InventoryUpdateQty to db: \n   {}'.format(sql)
        cursor.execute(sql)
        conn.commit()
        conn.close()
    # except Exception as e:
    #     print 'Failed to execute sql query: {}'.format(e)
    #     return e


def add_inventory_update_bin(user_id, item_number, loc_code, bin_current, bin_new):
    system_id = config_util.get_config('general', 'system_id', '12345')
    system_name = config_util.get_config('general', 'name', 'ABM Barcode')
    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if True:
    # try:
        conn = connect()
        if conn is None:
            return 'Failed to connect'
        cursor = conn.cursor()
        if bin_current == '':
            sql = "INSERT INTO T_Bar_InventoryUpdateBin (SystemID, SystemName, DateTime, UserID, ItemNumber, " \
                  "LOCNCODE, BINNMBRNew) VALUES " \
                  "('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', '{}', '{}');".format(
                   system_id, system_name, dt, user_id, item_number, loc_code, bin_new)
        else:
            sql = "INSERT INTO T_Bar_InventoryUpdateBin (SystemID, SystemName, DateTime, UserID, ItemNumber, " \
                  "LOCNCODE, BINNMBRCurrent, BINNMBRNew) VALUES " \
                  "('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', '{}', '{}', '{}');".format(
                    system_id, system_name, dt, user_id, item_number, loc_code,
                    bin_current, bin_new)
        print 'Uploading InventoryUpdateBin to db: \n   {}'.format(sql)
        cursor.execute(sql)
        conn.commit()
        conn.close()
    # except Exception as e:
    #     print 'Failed to execute sql query: {}'.format(e)
    #     return e


def add_print_track(item_number, user_id):
    """
    Add print information
    :param item_number:
    :param user_id:
    :return:
    """
    system_id = config_util.get_config('general', 'system_id', '12345')
    system_name = config_util.get_config('general', 'name', 'ABM Barcode')
    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if True:
        # try:
        conn = connect()
        if conn is None:
            return 'Failed to connect'
        cursor = conn.cursor()
        sql = "INSERT INTO T_Bar_Receiving (SystemID, SystemName, DateTime, UserID, ItemNumber, Qty) VALUES " \
              "('{}', '{}', CAST(N'{}' AS DateTime), '{}', '{}', 1);".format(
                system_id, system_name, dt, user_id, item_number)
        print 'Adding print information: \n   {}'.format(sql)
        cursor.execute(sql)
        conn.commit()
        conn.close()


if __name__ == '__main__':
    print connect()
