import os
from kivy.uix.scrollview import ScrollView

from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.textfields import MDTextField

from widgets.dialog import InputDialog
from kivy.clock import Clock

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'textfield.kv'))


class NumericTextField(BoxLayout):

    hint_text = StringProperty('')
    value = NumericProperty(0)
    default_value = NumericProperty(0)
    allow_negative = BooleanProperty(False)
    icon_size = NumericProperty(30)

    def __init__(self, **kwargs):
        super(NumericTextField, self).__init__(**kwargs)
        if 'value' in kwargs.keys():
            self.set_value(kwargs['value'])

    def on_plus(self):
        self.value = int(self.ids.input.text)
        self.value += 1
        self.ids.input.text = str(self.value)

    def on_minus(self):
        self.value = int(self.ids.input.text)
        if self.value > 0 or self.allow_negative:
            self.value -= 1
            self.ids.input.text = str(self.value)
        else:
            self.value = 0
            self.ids.input.text = str(self.value)

    def set_value(self, val):
        try:
            self.value = int(val)
            self.ids.input.text = str(val)
        except ValueError:
            raise ValueError('Error, must be numeric')

    def get_value(self):
        return int(self.ids.input.text)

    def clear(self):
        self.set_value(self.default_value)

    def on_value(self, *args):
        self.set_value(args[1])

    def on_icon_size(self, *args):
        self.icon_size = args[1]


class ABMMDTextField(MDTextField):
    ignore_clear = BooleanProperty(False)
    ignore_validate = BooleanProperty(False)
    use_dlg = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(ABMMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = InputDialog(text=self.text, hint_text=self.hint_text, input_filter=self.input_filter)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')


class ScrollableTextInput(ScrollView):
    text = StringProperty('')
    font_size = NumericProperty(14)


if __name__ == '__main__':
    from kivy.app import App
    from kivymd.theming import ThemeManager

    class TextApp(App):
        theme_cls = ThemeManager()

        def build(self):
            # self.theme_cls.primary_palette = 'Indigo'
            return Builder.load_string("""
BoxLayout:
    spacing: '64dp'
    NumericTextField:
        hint_text: 'Hint Text'
        value: 4
""")


    TextApp().run()
