import os
import threading

from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from functools import partial
from kivy.uix.widget import Widget
from kivymd.button import MDIconButton

from utils.db import get_item_description

Builder.load_file(os.path.join(os.path.dirname(__file__), '_item_number.kv'))


class ItemNumberWidget(BoxLayout):

    item_number = StringProperty('')
    quantity = NumericProperty(0)

    def __init__(self, **kwargs):
        super(ItemNumberWidget, self).__init__(**kwargs)
        Clock.schedule_once(self.update_values)
        self.register_event_type('on_focus_item')

    def update_values(self, *args):
        self.ids.item_number.set_value(self.item_number)
        self.ids.quantity.set_value(self.quantity)

    def set_item_number(self, val):
        self.item_number = val
        self.update_values()

    def get_value(self):
        qty = self.ids.quantity.get_value()
        number = self.ids.item_number.get_value()
        if len(number) > 0 and qty != 0:
            return {
                'item_number': number,
                'quantity': qty
            }

    def clear(self):
        self.ids.item_number.clear()
        self.ids.quantity.clear()

    def on_focus_item(self, *args):
        pass

    def on_focus_item_number(self, *args):
        self.dispatch('on_focus_item', *args)


class ItemWidget(BoxLayout):

    item_number = StringProperty('')

    def __init__(self, **kwargs):
        super(ItemWidget, self).__init__(**kwargs)
        self.register_event_type('on_delete_me')
        self.register_event_type('on_add_another')
        Clock.schedule_once(self.update_values)

    def update_values(self, *args):
        self.ids.txt.set_item_number(self.item_number)
        self.ids.txt.ids.item_number.bind(on_text_validate=self.on_item_number_validate)
        self.ids.txt.bind(on_focus_item=self.on_item_number_focus)
        if self.item_number != '':
            threading.Thread(target=self.pull_item_desc).start()

    def on_item_number_validate(self, instance, *args):
        item_number = instance.text
        if len(item_number) == 0:
            return
        self.item_number = item_number
        threading.Thread(target=self.pull_item_desc).start()

    def on_item_number_focus(self, *args):
        instance = args[0].ids.item_number
        if not instance.focus:
            self.on_item_number_validate(instance)

    def on_delete_me(self, *args):
        pass

    def on_add_another(self, *args):
        pass

    def remove_down_arrow(self):
        main_box = self.ids.main_box
        main_box.remove_widget(main_box.children[0])
        main_box.add_widget(Widget(size_hint_x=None, width=48))

    def add_down_arrow(self):
        main_box = self.ids.main_box
        btn_add = MDIconButton(icon='arrow-down', pos_hint={'center_y': .6})
        btn_add.bind(on_release=self.on_btn_add)
        main_box.remove_widget(main_box.children[0])
        main_box.add_widget(btn_add)

    def on_btn_add(self, *args):
        self.dispatch('on_add_another', '')

    def clear(self):
        self.ids.txt.clear()

    def get_value(self):
        return self.ids.txt.get_value()

    def set_value(self, val):
        self.ids.txt.set_value(val)

    def pull_item_desc(self, *args):
        item_desc = get_item_description(self.item_number)
        Clock.schedule_once(partial(self.update_item_desc, item_desc))

    @mainthread
    def update_item_desc(self, item_desc, *args):
        print 'Got item description of {} from DB: {}'.format(self.item_number, item_desc)
        self.ids.desc.text = '{}'.format(item_desc)
