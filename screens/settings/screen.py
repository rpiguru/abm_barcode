import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty

from kivy.uix.screenmanager import Screen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'settings.kv'))


class SettingsScreen(Screen):

    cur_tab = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(SettingsScreen, self).__init__(**kwargs)
        Clock.schedule_once(self.initialize)

    def initialize(self, *args):
        self.cur_tab = self.ids.tab_general

    def on_enter(self, *args):
        super(SettingsScreen, self).on_enter(*args)
        if self.cur_tab is not None:
            self.cur_tab.on_enter()

    def on_leave(self, *args):
        self.cur_tab.on_leave()
        super(SettingsScreen, self).on_leave(*args)
