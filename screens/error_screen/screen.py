# -*- coding: iso8859-15 -*-
import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.config import _is_rpi

Builder.load_file(os.path.join(os.path.dirname(__file__), 'error.kv'))


class ErrorScreen(Screen):

    def on_enter(self, *args):
        error_msg = App.get_running_app().get_exception()
        self.ids.error_msg.text = str(error_msg)
        super(ErrorScreen, self).on_enter(*args)

    def on_touch_down(self, touch):
        super(ErrorScreen, self).on_touch_down(touch)
        if self.collide_point(*touch.pos) and _is_rpi:
            App.get_running_app().go_screen('menu', 'right')
