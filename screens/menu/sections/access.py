import os

import datetime
from kivy.lang import Builder
from kivy.properties import NumericProperty

from kivymd.snackbar import Snackbar
from screens.menu.sections.base import BaseSection
import time
from kivy.clock import Clock
import utils.db

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'access.kv'))


class AccessSection(BaseSection):

    item_list = ['user_id', 'user_action', 'timestamp']
    mode = 'Access'

    last_scan = NumericProperty(0)
    cnt = NumericProperty(0)

    def __init__(self, **kwargs):
        super(AccessSection, self).__init__(**kwargs)

    def clear_fields(self):
        super(AccessSection, self).clear_fields()
        self.ids.user_action.enable(False)
        self.last_scan = 0
        self.ids.counter.text = ''
        self.cnt = 0
        Clock.unschedule(self.count_down)

    def set_item_value(self, _key, value, ignore_error=False):
        if _key == 'user_id':
            if self.last_scan > 0:
                if self.ids.user_action.get_value() is None:
                    self.unknown_access()
                else:
                    super(AccessSection, self).on_accept()
                Clock.unschedule(self.count_down)       # Mandatory, otherwise 2 counters will run at the same time
            self.ids.user_action.enable(True)
            self.last_scan = time.time()
            self.cnt = 0
            self.ids.counter.text = '60'
            Clock.schedule_interval(self.count_down, 1)
        super(AccessSection, self).set_item_value(_key=_key, value=value, ignore_error=ignore_error)

    def unknown_access(self):
        data = {
            'user_id': self.ids.user_id.get_value(),
            'timestamp': self.ids.timestamp.get_value(),
            'user_action': 'Unknown',
            'mode': self.mode,
        }
        result, state = utils.db.upload_data(data)
        if result:
            Snackbar(text='Uploaded unknown data.').show()
            self.move_to_historical_area(data=data)
            return True
        else:
            return False

    def count_down(self, *args):
        if self.cnt < 60:
            self.cnt += 1
            self.ids.counter.text = str(60 - self.cnt)
        else:
            if self.ids.user_action.get_value() is None:
                if self.unknown_access():
                    Clock.unschedule(self.count_down)
                    self.clear_fields()
                else:   # Restart counter
                    self.cnt = 0
                    self.ids.counter.text = '60'
            else:
                self.on_accept()

    def on_accept(self):
        super(AccessSection, self).on_accept()
        # If successfully upload, `last_scan` will be 0
        if self.last_scan == 0:
            Clock.unschedule(self.count_down)
        else:
            self.cnt = 0
            self.ids.counter.text = ''
