import os
import threading
from functools import partial
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder
from kivy.properties import DictProperty, StringProperty, ObjectProperty
from kivymd.dialog import MDDialog
from kivy.clock import Clock, mainthread
from kivymd.snackbar import Snackbar
from kivymd.textfields import MDTextField
from utils.db import add_inventory_update_qty, add_inventory_update_bin
from kivy.config import _is_rpi

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'inventory_item.kv'))


class InventoryItemPopup(MDDialog):

    title = 'Inventory Item Detail'
    detail = DictProperty()
    user_id = StringProperty()
    screen = ObjectProperty()

    def __init__(self, **kwargs):
        super(InventoryItemPopup, self).__init__(**kwargs)
        self._container.parent.parent.padding = (24, 24, 24, 12)
        self.register_event_type('on_item_updated')
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        for _key in self.detail.keys():
            try:
                self.ids[_key].text = ' '.join(str(self.detail[_key]).splitlines())
                if isinstance(self.ids[_key], MDTextField):
                    self.ids[_key].cursor = (0, 0)
            except (ReferenceError, KeyError):
                pass

        self.ids.user_id.text = self.user_id

        grid = self.ids.grid
        for b in self.detail['bin_loc']:
            box = BinItem(bin_info=b)
            grid.add_widget(box)
            grid.height += box.height
        grid.height += 10

    def on_btn_ok(self):
        if self.ids['user_id'].text == '' and _is_rpi:
            Snackbar(text="Please input User ID.", background_color=(.8, 0, .3, .5)).show()
            return
        self.screen.loading_dlg.open()
        if self.ids.QtyOHPending.text not in ['', '0']:
            print 'Updating T_Br_InventoryUpdateQty'
            threading.Thread(target=self.update_qty).start()
        else:
            threading.Thread(target=self.update_bin_locations).start()

    def update_qty(self, *args):
        result = add_inventory_update_qty(user_id=self.user_id, item_number=self.detail['ITEMNMBR'],
                                          qty_current=self.detail['QTYONHND00'],
                                          qty_new=self.ids.QtyOHPending.text)
        if result is not None:
            Clock.schedule_once(partial(self.show_error, result))
        else:
            threading.Thread(target=self.update_bin_locations).start()

    @mainthread
    def show_error(self, msg, *args):
        self.screen.loading_dlg.dismiss()
        Snackbar(text="Failed to update: {}".format(msg), background_color=(.8, 0, .3, .5)).show()

    def update_bin_locations(self, *args):
        results = []
        for i, box in enumerate(self.ids.grid.children):
            new_val = box.ids.BinLocPending.text
            if new_val != '':
                r = add_inventory_update_bin(user_id=self.user_id, item_number=self.detail['ITEMNMBR'],
                                             loc_code=self.detail['bin_loc'][i]['LOCNCODE'],
                                             bin_current=self.detail['bin_loc'][i]['BINNMBR'],
                                             bin_new=new_val)
                results.append(r)

        if any(results):
            Clock.schedule_once(partial(self.show_error, [r for r in results if r][0]))
        else:
            self.screen.loading_dlg.dismiss()
            self.dismiss()
            self.dispatch('on_item_updated')

    def update_user_id(self, val):
        self.user_id = val
        self.ids.user_id.text = val

    def on_item_updated(self, *args):
        pass


class BinItem(BoxLayout):

    bin_info = DictProperty()

    def __init__(self, **kwargs):
        super(BinItem, self).__init__(**kwargs)
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        for _key in self.bin_info.keys():
            self.ids[_key].text = self.bin_info[_key]
