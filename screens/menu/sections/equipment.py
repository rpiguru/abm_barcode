import os

import time
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder
from screens.menu.sections.base import BaseSection
from widgets.dialog import TextInputDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'equipment.kv'))


class EquipmentSection(BaseSection):

    item_list = ['user_id', 'job_number', 'job_name', 'equipment_id', 'note',
                 'customer_name', 'division', 'timestamp'
                 ]
    mode = 'Equipment'

    def on_chk_status(self, widget, status):
        if widget.active:
            if status == 'B':
                self.open_note_dlg()

    def open_note_dlg(self):
        popup = TextInputDialog(text=self.ids.note.text)
        popup.bind(on_confirm=self.on_confirm_note)
        popup.open()

    def on_confirm_note(self, *args):
        self.ids.note.text = args[1]

    def _collect_data(self):
        """
        Add `status` value
        :return:
        """
        data = super(EquipmentSection, self)._collect_data()
        for s in ['R', 'T', 'B']:
            if self.ids['chk_{}'.format(s)].active:
                data['status'] = s
                break
        return data

    def update_status_label(self, status, *args):
        wid = self.ids.eq_status
        wid.text = {'T': 'Out', 'R': 'In', 'B': 'Broken'}[status]

    def clear_fields(self):
        self.ids.note.text = ''
        self.ids.description.text = ''
        self.ids.eq_status.text = ''
        for s in ['R', 'T', 'B']:
            self.ids['chk_{}'.format(s)].active = False
        super(EquipmentSection, self).clear_fields()

    def custom_validate(self):
        if not self.ids.chk_T.active and not self.ids.chk_R.active and not self.ids.chk_B.active:
            return ['Please select Equipment Status']
        else:
            return []
