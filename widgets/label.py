import os

from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView

from widgets.base import ABMWidgetBase

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'label.kv'))


class LabelBase(ABMWidgetBase):

    def get_value(self):
        return self.ids.label.text

    def set_value(self, val):
        self.ids.label.text = val

    def clear(self):
        self.set_value('')


class TitledLabel(BoxLayout, LabelBase):

    title = StringProperty('')
    text = StringProperty('')


class ScrollableLabel(ScrollView, LabelBase):
    text = StringProperty('')


if __name__ == '__main__':
    from kivy.app import App
    from kivymd.theming import ThemeManager

    class LabelApp(App):
        theme_cls = ThemeManager()

        def build(self):
            # self.theme_cls.primary_palette = 'Indigo'
            return Builder.load_string("""
BoxLayout:
    spacing: '64dp'
    TitledLabel:
        title: 'Test Title'
        text: 'aa'
""")

    LabelApp().run()