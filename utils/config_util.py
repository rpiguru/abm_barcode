"""
    Utility for configuration
"""
import ConfigParser
import os

config = ConfigParser.RawConfigParser()
config_file = os.path.expanduser('~/.abm_barcode.ini')


def get_config(section, option, default_value=None):
    """
    Get configuration value
    :param section:
    :param option:
    :param default_value:
    :return:
    """
    config.read(config_file)
    try:
        val = config.get(section=section, option=option)
        if type(val) == list:
            val = val[0]
        return val
    except ConfigParser.NoSectionError:
        return default_value
    except ConfigParser.NoOptionError:
        return default_value


def set_config(section, option, value):
    if type(value) != str:
        value = str(value)

    config.read(config_file)
    if section not in config.sections():
        config.add_section(section=section)
    config.set(section, option, value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except IOError as e:
        # print('Failed to write default geo-location to file: {}'.format(e))
        return False
