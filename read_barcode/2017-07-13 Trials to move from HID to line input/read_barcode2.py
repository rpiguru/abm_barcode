import os
import time

hid = {4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k', 15: 'l',
       16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u', 25: 'v', 26: 'w', 27: 'x',
       28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5', 35: '6', 36: '7', 37: '8', 38: '9', 39: '0',
       # 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: '\', 51: ';', 52: "\", 53: '~', 54: ',', 55: '.', 56: '/'}
       44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: 'a', 51: ';', 52: "a", 53: '~', 54: ',', 55: '.', 56: '/'}

hid2 = {4: 'A', 5: 'B', 6: 'C', 7: 'D', 8: 'E', 9: 'F', 10: 'G', 11: 'H', 12: 'I', 13: 'J', 14: 'K', 15: 'L',
        16: 'M', 17: 'N', 18: 'O', 19: 'P', 20: 'Q', 21: 'R', 22: 'S', 23: 'T', 24: 'U', 25: 'V', 26: 'W', 27: 'X',
        28: 'Y', 29: 'Z', 30: '!', 31: '@', 32: '#', 33: '$', 34: '%', 35: '^', 36: '&', 37: '*', 38: '(', 39: ')',
        44: ' ', 45: '_', 46: '+', 47: '{', 48: '}', 49: '|', 51: ':', 52: '"', 53: '~', 54: '<', 55: '>', 56: '?'}

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

"""
Read something:  [2, 0, 12, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [2, 0, 23, 0, 0, 0, 0, 0]
Read something:  [2, 0, 0, 0, 0, 0, 0, 0]
Read something:  [0, 0, 30, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]
Read something:  [0, 0, 35, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]
Read something:  [0, 0, 39, 0, 0, 0, 0, 0]

Read something:  [0, 0, 32, 0, 0, 0, 0, 0]
Read something:  [0, 0, 33, 0, 0, 0, 0, 0]
Read something:  [0, 0, 31, 0, 0, 0, 0, 0]
Read something:  [0, 0, 40, 0, 0, 0, 0, 0]
Barcode read: IT!0600342
"""

scn = ''


def get_port_of_barcode_reader():
    """
    Get port name of read_barcode reader
    """
    pipe = os.popen('dmesg | grep hidraw')
    data = pipe.read().strip().splitlines()
    pipe.close()
    for line in data[::-1]:
        # print line
        # print line.split('hidraw')[1][0]
        if 'Barcode' in line:
            num = line.split('hidraw')[1][0]
            scn = 'Barcode'
            return '/dev/hidraw{}'.format(num)
        elif "NETUM" in line:
            num = line.split('hidraw')[1][0]
            return '/dev/hidraw{}'.format(num)
        elif "CT1016K01962" in line:
            num = line.split('hidraw')[1][0]
            return '/dev/hidraw{}'.format(num)

    return '/dev/hidraw0'


class BarcodeReader:
    port = '/dev/hidraw0'
    fp = None
    reverse = False
    len = 8
    index = 2

    def __init__(self, port=None, reverse=False, len=8, index=3):

        if port is None:
            self.port = get_port_of_barcode_reader()
            # print scnr
            # print self.port
        else:
            self.port = port
        # print self.port
        self.reverse = reverse

        self.index = index
        # print self.port
        self.fp = open(self.port, 'rb')
        # print " isa"

    def read_barcode(self):
        ss = ""
        done = False
        buf = None

        while not done:
            try:
                # Get the character from the HID
                buf = self.fp.read(self.len)
                print 'Barcode buf: {}'.format(str(buf))
                if len(buf) > 0:
                    buf = [int(ord(c)) for c in buf]
                    #                    print 'Read something: ', buf
                    print "isa"
                    print self.index

                    if self.reverse:
                        buf.reverse()
                    if buf[self.index] == 40:
                        done = True
                        break
                    elif buf[self.index] > 0:
                        if buf[self.index - 2] == 0:
                            ss += hid[buf[self.index]]
                        elif buf[self.index - 2] == 2:
                            ss += hid2[buf[self.index]]
                        elif buf[self.index - 3] == 0:
                            ss += hid[buf[self.index]]
                        elif buf[self.index - 3] == 2:
                            ss += hid2[buf[self.index]]
            except (KeyError, ValueError, IndexError):
                print 'Invalid barcode: {}'.format(buf)
                continue
        return ss


if __name__ == '__main__':

    # get_port_of_barcode_reader()
    # print self.port

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    while True:
        try:
            # port='/dev/hidraw2'
            a = BarcodeReader(port=None, reverse=False, len=8, index=3)

        except IOError:
            time.sleep(1)
            continue
        else:
            code = a.read_barcode()
            print 'Barcode read: {}'.format(code)
            if code is not None:
                print 'Barcode read: {}'.format(code)
            else:
                pass
