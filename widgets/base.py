from kivy.properties import BooleanProperty
from kivy.uix.widget import Widget


class ABMWidgetBase(Widget):

    ignore_clear = BooleanProperty(False)
