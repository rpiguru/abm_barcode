import os
from kivy.lang import Builder
from kivy.clock import Clock
from functools import partial

from kivy.uix.widget import Widget

from screens.menu.sections.base import BaseSection
from widgets.inventory_allocation.item_number import ItemWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'inventory_allocation.kv'))


class IASection(BaseSection):

    item_list = ['user_id', 'job_number', 'job_name',
                 'customer_name', 'division', 'timestamp'
                 ]
    mode = 'Inventory Allocation'

    def __init__(self, **kwargs):
        super(IASection, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(IASection, self).on_enter(*args)
        Clock.schedule_once(self.update_constants)

    def update_constants(self, *args):
        pass

    def add_item(self, item_number='', *args):
        container = self.ids.container
        if len(container.children) > 0:
            container.remove_widget(container.children[0])
            # Remove `add` button
            try:
                last_item = container.children[0]
                if isinstance(last_item, ItemWidget):
                    last_item.remove_down_arrow()
            except IndexError:
                pass
        _item = ItemWidget(item_number=item_number)
        _item.bind(on_delete_me=partial(self.delete_item, _item))
        _item.bind(on_add_another=partial(self.add_item, ''))
        # if item_number != '':
        #     threading.Thread(target=self.pull_item_desc, args=(_item, item_number)).start()
        container.add_widget(_item)
        container.add_widget(Widget())
        container.height = 55 * len(container.children)

    def delete_item(self, item, *args):
        _parent = item.parent
        index = _parent.children.index(item)
        _parent.remove_widget(item)
        # Add `add` button to the last item
        if index == 1 and len(_parent.children) > 1:
            last_item = _parent.children[1]
            last_item.add_down_arrow()

        if len(self.ids.container.children) == 1:
            self.enable_add_btn(True)
        self.ids.container.height = 55 * len(self.ids.container.children)

    def clear_fields(self):
        self.ids.container.clear_widgets()
        self.enable_add_btn(True)
        super(IASection, self).clear_fields()

    def set_item_value(self, _key, value, ignore_error=False):
        if _key == 'item_number':
            self.add_item(value)
            self.enable_add_btn(False)
        else:
            super(IASection, self).set_item_value(_key=_key, value=value, ignore_error=ignore_error)

    def _collect_data(self):
        data = {}
        for _item in self.item_list:
            data[_item] = self.ids[_item].get_value()
        data['mode'] = self.mode
        item_list = []
        for widget in [_widget for _widget in self.ids.container.walk(restrict=True)
                       if isinstance(_widget, ItemWidget)]:
            _item_val = widget.get_value()
            if _item_val is not None:
                item_list.append(_item_val)
        data['item'] = item_list
        return data

    def custom_validate(self):
        if len(self._collect_data()['item']) == 0:
            return ['Invalid Item Number values']
        else:
            for widget in [_widget for _widget in self.ids.container.walk(restrict=True)
                           if isinstance(_widget, ItemWidget)]:
                if widget.get_value() is None:
                    return ['An item has zero quantity']
            return []

    def on_add_item(self, wid):
        self.ids.container.clear_widgets()
        self.add_item('')
        self.enable_add_btn(False)

    def enable_add_btn(self, en):
        self.ids.btn_add.opacity = 1 if en else 0
        self.ids.btn_add.disabled = False if en else 0
        self.ids.btn_add.height = 50 if en else 0
