import os
from kivy.lang import Builder
from kivymd.theme_picker import MDThemePicker
from screens.settings.tabs.base import BaseTab
from kivy.clock import Clock

from utils.common import set_theme_color

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'theme.kv'))


class ThemeTab(BaseTab):

    def on_btn(self):
        picker = MDThemePicker()
        picker.bind(on_dismiss=self._save_theme)
        Clock.schedule_once(picker.open)

    def _save_theme(self, *args):
        theme = {
            'primary_palette': self.theme_cls.primary_palette,
            'accent_palette': self.theme_cls.accent_palette,
            'theme_style': self.theme_cls.theme_style
        }
        set_theme_color(theme)
