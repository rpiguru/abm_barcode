import datetime
from kivy.config import _is_rpi
from reportlab.pdfgen import canvas
import barcode
from barcode.writer import *
from reportlab.lib.units import mm
from utils.config_util import get_config

if _is_rpi:
    import cups


class MyBarcodeWriter(ImageWriter):

    def __init__(self):
        super(MyBarcodeWriter, self).__init__()
        self.dpi = 400

    def _paint_text(self, xpos, ypos):
        """
        Re-define this function to avoid printing barcode value below barcode image.
        :param xpos:
        :param ypos:
        :return:
        """
        pass


def print_file(file_name, copies=1):
    if _is_rpi:
        try:
            conn = cups.Connection()
            printers = conn.getPrinters()
            printer_name = get_current_printer_name(printers)
            result = conn.printFile(printer_name, file_name, 'Print label', {'copies': str(copies), 'collate': 'true'})
            print 'Printing `{}`: {} '.format(file_name, result)
            return result
        except Exception as e:
            print 'Failed to print: {}'.format(e)
    else:
        print ''


def get_current_printer_name(printers):
    """
    Return currently connected printer name
    :return:
    """
    for _key, _item in printers.items():
        if str(_item['device-uri']).startswith('usb://'):
            return _key


def create_barcode_png(code='JB153100F'):
    CODE39 = barcode.get_barcode_class('code39')
    ean = CODE39(code, writer=MyBarcodeWriter(), add_checksum=False)
    fullname = ean.save(code)
    return fullname


def create_pdf(customer_name='Akman Constructions', job_name='Edgewood Estates', _barcode='153100F',
               div_num='20 SERVICE'):
    """
    Create label with given parameters
    :param div_num:
    :param customer_name:
    :param job_name:
    :param _barcode:
    :return:
    """
    is_small = (get_config('general', 'small_label_print', 'False').lower() == 'true')
    if is_small:
        return create_small_label_pdf(customer_name, job_name, _barcode, div_num)
    job_barcode = 'JB{}'.format(_barcode)
    # Create read_barcode image
    barcode_img = create_barcode_png(job_barcode)

    width, height = 95 * mm, 59 * mm

    c = canvas.Canvas('result.pdf', pagesize=(width, height))

    # c.setFillColorRGB(0, 0, 255)

    # Draw the barcode image
    c.drawImage(barcode_img, 5, 0, width=270, height=70)
    c.drawString(100, 8, job_barcode)
    c.line(100, 7, 100 + 6.8 * len(job_barcode), 7)
    c.setFontSize(16)
    c.drawString(15, 107, customer_name)
    c.drawString(15, 83, job_name)
    c.drawString(width - 11 * len(str(div_num)), 135, '{}'.format(div_num))

    c.setFontSize(21)
    c.drawString(15, 135, _barcode)

    c.save()
    if _is_rpi:
        os.remove(barcode_img)

    return 'result.pdf'


def create_small_label_pdf(customer_name='Akman Constructions', job_name='Edgewood Estates',
                           _barcode='153100F', div_num='20 SERVICE'):
    print 'Creating PDF file in small size.'
    job_barcode = 'JB{}'.format(_barcode)
    # Create read_barcode image
    barcode_img = create_barcode_png(job_barcode)

    width, height = 89 * mm, 29 * mm

    c = canvas.Canvas('result.pdf', pagesize=(width, height))

    # c.setFillColorRGB(0, 0, 255)

    # Draw the barcode image
    c.drawImage(barcode_img, -7, 0, width=230, height=20)

    c.setFontSize(13)
    c.drawString(5, 43, customer_name)
    c.drawString(5, 30, job_name)
    c.drawString(width - 11 * len(str(div_num)), 60, '{}'.format(div_num))
    c.setFontSize(21)
    c.drawString(5, 57, _barcode)

    c.save()
    if _is_rpi:
        os.remove(barcode_img)

    return 'result.pdf'


def create_inventory_pdf(part_num, part_desc, bin_loc):

    width, height = 89 * mm, 29 * mm

    c = canvas.Canvas('result.pdf', pagesize=(width, height))

    # c.setFillColorRGB(0, 0, 255)

    # Draw Part Number
    c.setFontSize(24)
    c.drawString(0, 55, str(part_num))

    # Draw Part Description
    if len(part_desc) > 31:
        c.setFontSize(12)
        c.drawString(0, 36, str(part_desc[:31]))
        c.drawString(0, 24, str(part_desc[31:]))
    else:
        c.setFontSize(14)
        c.drawString(1, 30, str(part_desc))

    # Draw Bin/Loc pairs
    c.setFontSize(10)
    c.drawString(1, 8, 'Bin: ')
    c.drawString(30, 8, str(bin_loc))

    # Draw datetime
    c.drawString(180, 8, datetime.datetime.now().strftime('%Y-%m-%d'))

    c.save()

    return 'result.pdf'


if __name__ == '__main__':
    # create_barcode_png()
    # create_pdf()
    # print_file('result.pdf', copies=2)
    # create_inventory_pdf(part_num='10650014', part_desc='DEAD BOLT SCHLAGE D/C ANBS' * 2, bin_loc='WH/BIN WH/BIN')
    create_small_label_pdf()
