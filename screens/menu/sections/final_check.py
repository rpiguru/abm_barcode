import os
import threading

from kivy.clock import mainthread
from kivy.lang import Builder
from kivymd.card import MDSeparator

from screens.menu.sections.base import BaseSection
from utils.db import get_fc_verification_list
from widgets.dialog import TextInputDialog, FCVerificationDialog
from widgets.final_check.widget import FCItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'final_check.kv'))


class FinalCheckSection(BaseSection):

    item_list = ['user_id', 'product_number', 'job_name', 'note',
                 'customer_name', 'division', 'timestamp', 'sequence_number',
                 ]
    mode = 'Final Check'

    v_list = []

    def on_enter(self, *args):
        super(FinalCheckSection, self).on_enter(*args)
        self.v_list = get_fc_verification_list()
        self.load_verification_list()

    @mainthread
    def load_verification_list(self, *args):
        container = self.ids.container
        container.clear_widgets()
        for v in self.v_list:
            container.add_widget(FCItem(desc=v['FCDesc'], fc_id=v['FCID'], _id=v['ID'], value=v.get('value')))
            container.add_widget(MDSeparator(height=1))
        container.height = FCItem().height * len(self.v_list) + 5

    def custom_validate(self):
        for wid in [w for w in self.ids.container.children if not isinstance(w, MDSeparator)]:
            if wid.get_value() is None:
                return ['Please check all items']
        return []

    def _collect_data(self):
        data = {}
        for _item in self.item_list:
            data[_item] = self.ids[_item].get_value()
        data['mode'] = self.mode
        item_list = []
        for widget in [w for w in self.ids.container.children if not isinstance(w, MDSeparator)]:
            _item_val = widget.get_value()
            if _item_val is not None:
                item_list.append({
                    'fc_id': widget.fc_id, 'status': _item_val,
                })
        data['item'] = item_list
        return data

    def open_check_dlg(self):
        popup = FCVerificationDialog(v_list=self.v_list)
        popup.bind(on_confirm=self.on_check_items)
        popup.open()

    def open_note_dlg(self):
        popup = TextInputDialog(text=self.ids.note.text)
        popup.bind(on_confirm=self.on_confirm_note)
        popup.open()

    def on_confirm_note(self, *args):
        self.ids.note.text = args[1]   # Max length is 50

    def on_focus_job_number(self, *args):
        if not self.ids.product_number.focus:
            self.btn_job_number()

    def on_accept(self):
        if len(self.custom_validate()) > 0:
            self.open_check_dlg()
        else:
            super(FinalCheckSection, self).on_accept()

    def btn_job_number(self, check_blank=True):
        job_num = self.ids.product_number.get_value()
        if len(job_num.strip()) == 0 and check_blank:
            # Snackbar(text='Please input job number', background_color=(.8, 0, .3, .5)).show()
            return
        threading.Thread(target=self.pull_job_desc, args=(job_num,)).start()

    def on_check_items(self, *args):
        self.v_list = args[1]
        self.load_verification_list()

    def on_btn_check(self):
        self.open_check_dlg()

    def clear_fields(self):
        super(FinalCheckSection, self).clear_fields()
        for v in self.v_list:
            if 'value' in v.keys():
                v.pop('value')
        self.load_verification_list()
