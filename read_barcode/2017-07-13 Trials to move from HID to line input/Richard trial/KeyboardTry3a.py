from msvcrt import getch
import time
print "Press Keys"

#Done = 1

#while Done:
#    key = ord(getch())
#    if key == 27: #ESC
#        break
#    elif key == 13: #Enter
#        Done = 0
#    elif key == 224: #Special keys (arrows, f keys, ins, del, etc.)
#        key = ord(getch())
#        if key == 80: #Down arrow
#            #moveDown()
#            print "DOWN"
#        elif key == 72: #Up arrow
#            #moveUp()
#            print "UP"
#    #print key

barcode = ""

while True:
    key = ord(getch())
    if key != 0:
        if key == 13:
            print 'Barcode read: {}'.format(barcode)
            barcode=""
        else:
            barcode += chr(key)
