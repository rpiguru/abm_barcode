"""

    Common utilities

"""
import os
from kivy.app import App

from utils import config_util
import utils.db

history_file = os.path.expanduser('~/.abm_barcode_history.txt')


alias_map = {
    'user_id': 'User ID',
    'mode': 'Mode',
    'item_number': 'Item #',
    'job_number': 'Job #',
    'job_name': 'Job Name',
    'customer_name': 'Customer',
    'division': 'Division',
    'quantity': 'Quantity',
    'product_number': 'Product ID',
    'activity': 'Activity',
    'timestamp': 'Datetime',
    'user_action': 'Action',
    'sequence_number': 'Sequence #',
    'equipment_id': 'EQ ID',
}


def is_debug_mode():
    cur_mode = config_util.get_config('general', 'debug', 'OFF')
    is_debug = True if cur_mode.lower() == 'on' else False
    return is_debug


def is_debug_button():
    cur_mode = config_util.get_config('general', 'debug_button', 'OFF')
    return cur_mode.lower() == 'on'


def set_debug_button_mode(val):
    _val = 'ON' if val else 'OFF'
    config_util.set_config('general', 'debug_button', _val)


def set_activity(val):
    config_util.set_config('user', 'activity_code', val)


def get_activity():
    return config_util.get_config('user', 'activity_code', '')


def get_activity_code():
    activity = get_activity()
    try:
        code = utils.db.get_production_activities().index(activity)
        return code
    except ValueError:
        return 0


def get_mode():
    return config_util.get_config('general', 'mode', 'I')


def append_historic_data(new_data):
    try:
        with open(history_file, 'r') as f:
            f_content = f.read().splitlines()
    except IOError:
        f_content = []
    f_content = [new_data, ] + f_content[:1000]

    try:
        with open(history_file, 'w') as f:
            f.write('\n'.join(f_content))
    except Exception as e:
        print 'Failed to append new historic data: {}'.format(e)


def get_historic_data():
    try:
        with open(history_file, 'r') as f:
            f_content = f.read()
    except IOError:
        f_content = ''
    return f_content.splitlines()[:5]


def get_admin_timer():
    return int(config_util.get_config('admin', 'timer', 300))


def set_admin_timer(val):
    config_util.set_config('admin', 'timer', val)


def get_git_commit_date():
    pipe = os.popen('cd /home/pi/abm_barcode; git log -1')
    data = pipe.read().strip().splitlines()
    pipe.close()
    r = 'Not available'
    for d in data:
        if d.startswith('Date:'):
            r = ' '.join(d.split('Date:')[1].split(' ')[:-1])
    print '>>>>> Last Updated: {}'.format(r)
    return r


def get_theme_color():
    theme = {
        'primary_palette': config_util.get_config('theme', 'primary_palette', 'Blue'),
        'accent_palette': config_util.get_config('theme', 'accent_palette', 'Amber'),
        'theme_style': config_util.get_config('theme', 'theme_style', 'Light'),
    }
    return theme


def set_theme_color(val):
    for _key in val.keys():
        config_util.set_config('theme', _key, val[_key]),
