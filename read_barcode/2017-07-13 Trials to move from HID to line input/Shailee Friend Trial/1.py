from evdev import InputDevice, categorize, ecodes
import evdev
import thread
from threading import Thread

'''
Need to instal evdev package usign command "sudo pip install evdev"
'''


# Function for Reading data from HID
def readdata(iDevice, dName):
    print "Started for %s" % dName
    dev = InputDevice(iDevice)
    for event in dev.read_loop():
        if event.type == ecodes.EV_KEY:
            print(categorize(event))


# Get All atteched Device and start Reading thread
devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]

for device in devices:
    print "------------------------------------"
    print(device.fn, device.name, device.phys)
    try:
        t = Thread(target=readdata, args=(device.fn, device.name,))
        t.start()
    except:
        print "Error: unable to start thread"
    print "------------------------------------"
