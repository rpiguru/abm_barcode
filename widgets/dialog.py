import os
import threading
from kivy.uix.textinput import TextInput

from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ObjectProperty, ListProperty
from kivy.uix.modalview import ModalView
from kivymd.card import MDSeparator

import utils.net
from kivymd.dialog import MDDialog
import utils.net
from kivymd.label import MDLabel
from utils.common import get_admin_timer
from functools import partial
from widgets.final_check.widget import FCCheckItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class WiFiInfoDialog(MDDialog):

    ssid = StringProperty('')
    quality = NumericProperty(0)
    ip = StringProperty('')
    mac_addr = StringProperty('')

    def __init__(self, **kwargs):
        super(WiFiInfoDialog, self).__init__(**kwargs)

    def on_open(self):
        Clock.schedule_once(self.get_wifi_details)

    def get_wifi_details(self, *args):
        wifi = utils.net.get_detail_of_connected_net()
        self.ssid = wifi['ssid']
        self.quality = int(wifi['quality'])
        self.ip = wifi['ip']
        self.mac_addr = wifi['mac']


class WiFiConnectDialog(MDDialog):

    ssid = StringProperty('')
    pwd = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_done')
        super(WiFiConnectDialog, self).__init__(**kwargs)

    def on_done(self, *args):
        pass

    def on_yes(self, *args):
        self.pwd = self.ids.txt_pwd.text
        threading.Thread(target=self.connect_to_ap).start()

    def show_load(self, *args):
        self.ids.btn_yes.disabled = True
        self.ids.btn_no.disabled = True
        self.ids.txt_pwd.disabled = True
        self.ids.spinner.opacity = 1

    @mainthread
    def hide_load(self):
        self.ids.btn_yes.disabled = False
        self.ids.btn_no.disabled = False
        self.ids.txt_pwd.disabled = False
        self.ids.spinner.opacity = 0

    def connect_to_ap(self, *args):
        Clock.schedule_once(self.show_load)
        ip = utils.net.connect_to_ap('wlan0', self.ssid, self.pwd)
        Clock.schedule_once(partial(self.connect_callback, ip))

    @mainthread
    def connect_callback(self, ip, *args):
        self.hide_load()
        if ip is None:
            self.ids.container.add_widget(
                MDLabel(font_style='Body1', theme_text_color='Error', text="Failed to connect", halign='center'))
        else:
            self.dismiss()
            self.dispatch('on_done', ip)


class PasswordDialog(MDDialog):
    pwd = StringProperty('')
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_success')
        super(PasswordDialog, self).__init__(**kwargs)

    def on_open(self):
        self.clear_error()
        self.ids.txt_pwd.text = ''
        self.ids.txt_pwd.focus = True
        threshold = int(get_admin_timer()) / 60
        self.ids.hint.text = 'NOTE: Administrator privilege will be expired in {} minutes'.format(threshold)

    def on_success(self, *args):
        pass

    def on_ok(self):
        if self.ids.txt_pwd.text == self.pwd:
            self.dismiss()
            self.dispatch('on_success')
        else:
            self.is_error = True

    def clear_error(self):
        self.is_error = False

    def on_touch_down(self, touch):
        if self.is_error:
            self.ids.txt_pwd.text = ''
            self.is_error = False
        super(PasswordDialog, self).on_touch_down(touch)


class LoadingDialog(ModalView):
    pass


class YesNoDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class InputDialog(MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')
    input_filter = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class CustomTextInput(TextInput):

    max_length = NumericProperty(50)

    def insert_text(self, substring, from_undo=False):
        super(CustomTextInput, self).insert_text(substring, from_undo)
        self.text = self.text[:50]


class TextInputDialog(MDDialog):

    text = StringProperty('')
    max_length = NumericProperty(50)

    def __init__(self, **kwargs):
        super(TextInputDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class FCVerificationDialog(MDDialog):
    v_list = ListProperty()

    def __init__(self, **kwargs):
        super(FCVerificationDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')

    def on_open(self):
        container = self.ids.container
        container.clear_widgets()
        for v in self.v_list:
            container.add_widget(FCCheckItem(desc=v['FCDesc'], fc_id=v['FCID'], _id=v['ID'], value=v.get('value')))
            container.add_widget(MDSeparator(height=1))
        container.height = FCCheckItem().height * len(self.v_list) + 5
        super(FCVerificationDialog, self).on_open()

    def on_yes(self):
        self.v_list = []
        for wid in [w for w in self.ids.container.children if isinstance(w, FCCheckItem)][::-1]:
            self.v_list.append({
                'FCDesc': wid.desc,
                'FCID': wid.fc_id,
                'ID': wid._id,
                'value': wid.get_value()
            })
        self.dispatch('on_confirm', self.v_list)
        self.dismiss()

    def on_btn_all_ok(self):
        for wid in [w for w in self.ids.container.children if isinstance(w, FCCheckItem)]:
            wid.set_value(1)

    def on_confirm(self, *args):
        pass

    def hide_error(self, *args):
        self.ids.lb_error.opacity = 0

    def on_btn_arrow(self, direction):
        if direction == 'up':
            self.ids.scroll.scroll_y = min(1, self.ids.scroll.scroll_y + .3)
        else:
            self.ids.scroll.scroll_y = max(0, self.ids.scroll.scroll_y - .3)
