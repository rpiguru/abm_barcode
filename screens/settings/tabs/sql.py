import os
from kivy.lang import Builder
from kivymd.snackbar import Snackbar

from screens.settings.tabs.base import BaseTab
from utils.db import get_db_credentials, set_db_credentials

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'sql.kv'))


class SQLTab(BaseTab):

    def on_enter(self, *args):
        credentials = get_db_credentials()
        for _key in credentials:
            try:
                self.ids[_key].set_value(credentials[_key])
            except KeyError:
                pass

    def on_leave(self, *args):
        credentials = {}
        for _key in ['host', 'port', 'username', 'password', 'database', 'instance']:
            try:
                credentials[_key] = self.ids[_key].get_value()
            except KeyError:
                pass
        set_db_credentials(credential=credentials)
        Snackbar(text="Updated sql settings").show()
