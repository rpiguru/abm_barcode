import os

import datetime
import pytz
from kivy.lang import Builder
from screens.menu.sections.base import BaseSection
from utils import time_util

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'time_clock.kv'))


class TimeClockSection(BaseSection):
    item_list = ['user_id', 'user_action', 'timestamp']
    mode = 'Time Clock'

    def __init__(self, **kwargs):
        super(TimeClockSection, self).__init__(**kwargs)

    def custom_validate(self):
        str_timestamp = self.ids.timestamp.get_value()
        try:
            timestamp = datetime.datetime.strptime(str_timestamp, "%Y-%m-%d %H:%M:%S")
            timestamp = timestamp.replace(second=0, microsecond=0)
        except ValueError:
            return ['Invalid timestamp value']
        else:
            t_now = time_util.get_local_time()
            t_now = t_now.replace(second=0, microsecond=0)
            action = self.ids.user_action.get_value()
            if action == 'In' and t_now > timestamp:
                return ['Timestamp should be later when `In`']
            elif action == 'Out' and t_now < timestamp:
                return ['Timestamp should be earlier when `Out`']
            return []

    def clear_fields(self):
        super(TimeClockSection, self).clear_fields()
        self.ids.timestamp.disabled = True
        self.ids.user_action.enable(False)

    def set_item_value(self, _key, value, ignore_error=False):
        super(TimeClockSection, self).set_item_value(_key=_key, value=value, ignore_error=ignore_error)
        if _key == 'user_id':
            self.ids.timestamp.disabled = False
            self.ids.user_action.enable(True)
