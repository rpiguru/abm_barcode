import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, DictProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'checkbox.kv'))


class LabeledCheckbox(GridLayout):

    group = StringProperty('')
    allow_no_selection = BooleanProperty(True)
    text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_checked')
        super(LabeledCheckbox, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_chk)

    def bind_chk(self, *args):
        self.ids.chk.bind(active=self.on_released)

    def on_released(self, *args):
        if args[0].active:
            self.dispatch('on_checked')

    def on_checked(self):
        pass

    def set_value(self, val):
        self.ids.chk.active = val

    def get_value(self):
        return self.ids.chk.active

    def enable(self, value):
        self.ids.chk.disabled = not value


class CoupleRadioGroup(BoxLayout):

    labels = ListProperty(['', ''])

    def __init__(self, **kwargs):
        super(CoupleRadioGroup, self).__init__(**kwargs)

    def clear(self):
        self.ids.chk1.set_value(False)
        self.ids.chk2.set_value(False)

    def get_value(self):
        if self.ids.chk1.get_value():
            return self.labels[0]
        elif self.ids.chk2.get_value():
            return self.labels[1]
        else:
            return None

    def set_value(self, val):
        if val == self.labels[0]:
            self.ids.chk1.set_value(True)
            self.ids.chk2.set_value(False)
        elif val == self.labels[1]:
            self.ids.chk1.set_value(False)
            self.ids.chk2.set_value(True)
        else:
            self.ids.chk1.set_value(False)
            self.ids.chk2.set_value(False)

    def enable(self, value):
        self.ids.chk1.enable(value=value)
        self.ids.chk2.enable(value=value)
