import os
import threading
from functools import partial
from kivy.clock import mainthread, Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivymd.snackbar import Snackbar
from kivymd.textfields import MDTextField
from kivy.config import _is_rpi
from utils import time_util
import utils.db
from widgets.dialog import LoadingDialog
from widgets.textfield import ABMMDTextField

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseSection(Screen):

    mode = StringProperty('')
    root_widget = ObjectProperty(None)
    item_list = ListProperty()
    loading_dlg = None

    def __init__(self, **kwargs):
        super(BaseSection, self).__init__(**kwargs)
        self.loading_dlg = LoadingDialog()

    def on_enter(self, *args):
        super(BaseSection, self).on_enter()
        self.clear_fields()

    def on_cancel(self):
        """
        Erase all fields
        """
        self.clear_fields()

    def on_accept(self):
        """
        Upload to the remote MS SQL DB, and move content to historical area
        """
        errors = self._validate()
        if len(errors) > 0:
            print 'Failed to gather required information: {}'.format(errors)
            Snackbar(text=errors[0], background_color=(.8, 0, .3, .5)).show()
            return

        custom_errors = self.custom_validate()
        if len(custom_errors) > 0:
            print 'Found custom error(s): {}'.format(custom_errors)
            Snackbar(text=custom_errors[0], background_color=(.8, 0, .3, .5)).show()
            return

        if 'job_name' in self.item_list:
            if self.ids['job_name'].get_value().lower() == 'unknown' and _is_rpi:
                print 'Invalid Job Number'
                Snackbar(text='Invalid Job Number', background_color=(.8, 0, .3, .5)).show()
                return

        self.loading_dlg.open()
        threading.Thread(target=self.upload_to_db).start()
        return True

    def move_to_historical_area(self, data=None):
        if data is None:
            data = self._collect_data()
        threading.Thread(target=self.root_widget.add_historic_data, args=(data, )).start()

    def clear_fields(self):
        for cur_widget in [self.ids[_item] for _item in self.item_list]:
            if isinstance(cur_widget, ABMMDTextField):
                if not cur_widget.ignore_clear:
                    cur_widget.clear()
            else:
                cur_widget.clear()

    def set_item_value(self, _key, value, ignore_error=False):
        if _key in self.item_list:
            wid = self.ids[_key]
            wid.set_value(value)
            if isinstance(wid, MDTextField):
                wid.cursor = (0, 0)
        elif not ignore_error:
            print('Error, key must be in item list')

    def _validate(self):
        errors = []
        for _item in self.item_list:
            wid = self.ids[_item]
            if wid.get_value() in ['', 0, None]:
                if not hasattr(wid, 'ignore_validate') or not wid.ignore_validate:
                    errors.append('Please input {}'.format(_item))
        return errors

    def custom_validate(self):
        return []

    def _collect_data(self):
        data = {}
        for _item in self.item_list:
            data[_item] = self.ids[_item].get_value()
        data['mode'] = self.mode
        return data

    def set_timestamp(self):
        local_time = time_util.get_local_time()
        try:
            self.ids.timestamp.text = local_time.strftime("%Y-%m-%d %H:%M:%S")
        except AttributeError:
            pass

    def upload_to_db(self, *args):
        data = self._collect_data()
        result, reason = utils.db.upload_data(data)
        self.show_result(result=result, reason=reason)

    @mainthread
    def show_result(self, result, reason):
        self.loading_dlg.dismiss()
        if result:
            Snackbar(text='Successfully uploaded.').show()
            self.move_to_historical_area()
            self.clear_fields()
        else:
            Snackbar(text='Failed to upload data: {}'.format(reason), background_color=(.8, 0, .3, .5)).show()

    def btn_job_number(self, check_blank=True):
        job_num = self.ids.job_number.get_value()
        if len(job_num.strip()) == 0 and check_blank:
            # Snackbar(text='Please input job number', background_color=(.8, 0, .3, .5)).show()
            return
        threading.Thread(target=self.pull_job_desc, args=(job_num,)).start()

    def pull_job_desc(self, *args):
        val = args[0]
        job_desc = utils.db.get_job_description(val)
        Clock.schedule_once(partial(self.update_job_desc, job_desc))

    @mainthread
    def update_job_desc(self, job_desc, *args):
        print 'Got job description from DB: {}'.format(job_desc)
        for _key, _val in job_desc.items():
            self.set_item_value(_key=_key, value=_val, ignore_error=True)

    def on_focus_job_number(self, *args):
        if not self.ids.job_number.focus:
            self.btn_job_number()
